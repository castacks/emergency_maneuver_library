/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
#include "emergency_maneuver_library/loiter.h"
// Bring in gtest
#include <gtest/gtest.h>

using namespace ca;
using namespace emergency_maneuver_library;

TEST(EMERGENCY_MANEUVER_LIBRARY, Loiter) {
	/*default constructor*/
	Loiter loit0(1,2,3,0.0,150,30,0.0,true);
	EXPECT_DOUBLE_EQ(loit0.GetXPos(),1);
	EXPECT_DOUBLE_EQ(loit0.GetYPos(),2);
	EXPECT_DOUBLE_EQ(loit0.GetZPos(),3);
	EXPECT_DOUBLE_EQ(loit0.GetHead(),0.0);
	EXPECT_DOUBLE_EQ(loit0.GetHeadRate(),-30.0/150.0);
	EXPECT_DOUBLE_EQ(loit0.GetRadius(),150);
	EXPECT_DOUBLE_EQ(loit0.GetSpeed(),30);
	const double roll0 ( -atan((30.0*30.0)/(9.81*150)) );
	EXPECT_DOUBLE_EQ(loit0.GetRoll(),roll0);
	EXPECT_DOUBLE_EQ(loit0.GetCircumfence(),2.0*M_PI*150.0);
	EXPECT_DOUBLE_EQ(loit0.GetTime(),(2.0*M_PI*150.0)/30.0);
	EXPECT_TRUE(loit0.IsHover()==false);
	EXPECT_TRUE(loit0.IsClockwise());
	EXPECT_FLOAT_EQ(loit0.GetCenter()[0],1.0);
	EXPECT_FLOAT_EQ(loit0.GetCenter()[1],2.0-150.0);
	EXPECT_FLOAT_EQ(loit0.GetCenter()[2],3.0);
	
	// same loiter different orientation
	Loiter loit1(1,2,3,0.0,150,30,0.0,false);
	EXPECT_DOUBLE_EQ(loit1.GetXPos(),1);
	EXPECT_DOUBLE_EQ(loit1.GetYPos(),2);
	EXPECT_DOUBLE_EQ(loit1.GetZPos(),3);
	EXPECT_DOUBLE_EQ(loit1.GetHead(),0.0);
	EXPECT_DOUBLE_EQ(loit1.GetHeadRate(),30.0/150.0);
	EXPECT_DOUBLE_EQ(loit1.GetRadius(),150);
	EXPECT_DOUBLE_EQ(loit1.GetSpeed(),30);
	EXPECT_DOUBLE_EQ(loit1.GetRoll(),-roll0);
	EXPECT_DOUBLE_EQ(loit1.GetCircumfence(),2.0*M_PI*150.0);
	EXPECT_DOUBLE_EQ(loit1.GetTime(),(2.0*M_PI*150.0)/30.0);
	EXPECT_TRUE(loit1.IsHover()==false);
	EXPECT_TRUE(loit1.IsClockwise()==false);
	EXPECT_FLOAT_EQ(loit1.GetCenter()[0],1.0);
	EXPECT_FLOAT_EQ(loit1.GetCenter()[1],2.0+150.0);
	EXPECT_FLOAT_EQ(loit1.GetCenter()[2],3.0);
	
	// same loiter different heading
	Loiter loit2(1,2,3,M_PI/2.0,150,30,0.0,false);
	EXPECT_DOUBLE_EQ(loit2.GetXPos(),1);
	EXPECT_DOUBLE_EQ(loit2.GetYPos(),2);
	EXPECT_DOUBLE_EQ(loit2.GetZPos(),3);
	EXPECT_DOUBLE_EQ(loit2.GetHead(),M_PI/2.0);
	EXPECT_DOUBLE_EQ(loit2.GetHeadRate(),30.0/150.0);
	EXPECT_DOUBLE_EQ(loit2.GetRadius(),150);
	EXPECT_DOUBLE_EQ(loit2.GetSpeed(),30);
	EXPECT_DOUBLE_EQ(loit2.GetRoll(),-roll0);
	EXPECT_DOUBLE_EQ(loit2.GetCircumfence(),2.0*M_PI*150.0);
	EXPECT_DOUBLE_EQ(loit2.GetTime(),(2.0*M_PI*150.0)/30.0);
	EXPECT_TRUE(loit2.IsHover()==false);
	EXPECT_TRUE(loit2.IsClockwise()==false);
	EXPECT_FLOAT_EQ(loit2.GetCenter()[0],1.0-150);
	EXPECT_FLOAT_EQ(loit2.GetCenter()[1],2.0);
	EXPECT_FLOAT_EQ(loit2.GetCenter()[2],3.0);
	
	// same loiter different direction of rotation
	Loiter loit3(1,2,3,M_PI/2.0,150,30,0.0,true);
	EXPECT_DOUBLE_EQ(loit3.GetXPos(),1);
	EXPECT_DOUBLE_EQ(loit3.GetYPos(),2);
	EXPECT_DOUBLE_EQ(loit3.GetZPos(),3);
	EXPECT_DOUBLE_EQ(loit3.GetHead(),M_PI/2.0);
	EXPECT_DOUBLE_EQ(loit3.GetHeadRate(),-30.0/150.0);
	EXPECT_DOUBLE_EQ(loit3.GetRadius(),150);
	EXPECT_DOUBLE_EQ(loit3.GetSpeed(),30);
	EXPECT_DOUBLE_EQ(loit3.GetRoll(),roll0);
	EXPECT_DOUBLE_EQ(loit3.GetCircumfence(),2.0*M_PI*150.0);
	EXPECT_DOUBLE_EQ(loit3.GetTime(),(2.0*M_PI*150.0)/30.0);
	EXPECT_TRUE(loit3.IsHover()==false);
	EXPECT_TRUE(loit3.IsClockwise());
	EXPECT_FLOAT_EQ(loit3.GetCenter()[0],1.0+150);
	EXPECT_FLOAT_EQ(loit3.GetCenter()[1],2.0);
	EXPECT_FLOAT_EQ(loit3.GetCenter()[2],3.0);



    // test trajectory
    //Matlab loiter with function "get_traj_circle_phi0" and check with C++ implementation
    Loiter loit4(174.0120,297.2068,20.0,2.3708,300,30,0.4,false);
    Trajectory<StateFW> traj (loit4.GetClosedTrajectory(0.1));
    EXPECT_EQ(traj.Size(),630);
    EXPECT_DOUBLE_EQ(traj.Front().Pos()[0],174.0120);
    EXPECT_DOUBLE_EQ(traj.Front().Pos()[1],297.2068);
    EXPECT_DOUBLE_EQ(traj.Front().Pos()[2],20.0);
    EXPECT_DOUBLE_EQ(traj.Front().Heading(),2.3708);
    EXPECT_DOUBLE_EQ(traj.Front().SpeedXY(),30.0);
    EXPECT_DOUBLE_EQ(traj.Front().SpeedZ(),0.0);
    EXPECT_DOUBLE_EQ(traj.Front().HeadingRate(),30.0/300.0);
    EXPECT_DOUBLE_EQ(traj.Front().Time(),0.4);
    
    // last point
    const double tol (1e-3);
    EXPECT_NEAR(traj.Back().Pos()[0],174.0120,tol);
    EXPECT_NEAR(traj.Back().Pos()[1],297.2068,tol);
    EXPECT_NEAR(traj.Back().Pos()[2],20.0,tol);
    EXPECT_NEAR(traj.Back().Heading(),2.3708,tol);
    EXPECT_NEAR(traj.Back().SpeedXY(),30.0,tol);
    EXPECT_NEAR(traj.Back().SpeedZ(),0.0,tol);
    EXPECT_NEAR(traj.Back().HeadingRate(),30.0/300.0,tol);
    EXPECT_NEAR(traj.Back().Time(),63.2319,tol);

    // Next to last
    EXPECT_NEAR((traj.End()-2)->Pos()[0],174.6965,tol);
    EXPECT_NEAR((traj.End()-2)->Pos()[1],296.5400,tol);
    EXPECT_NEAR((traj.End()-2)->Pos()[2],20.0,tol);
    //TODO wrap heading
    EXPECT_NEAR((traj.End()-2)->Heading(),8.6508,tol);
    EXPECT_NEAR((traj.End()-2)->SpeedXY(),30.0,tol);
    EXPECT_NEAR((traj.End()-2)->SpeedZ(),0.0,tol);
    EXPECT_NEAR((traj.End()-2)->HeadingRate(),30.0/300.0,tol);
    EXPECT_NEAR((traj.End()-2)->Time(),63.2000,tol);
    
    // one more poit before
    EXPECT_NEAR((traj.End()-3)->Pos()[0],176.8313,tol);
    EXPECT_NEAR((traj.End()-3)->Pos()[1],294.4323,tol);
    EXPECT_NEAR((traj.End()-3)->Pos()[2],20.0,tol);
    //TODO wrap heading
    EXPECT_NEAR((traj.End()-3)->Heading(),8.6408,tol);
    EXPECT_NEAR((traj.End()-3)->SpeedXY(),30.0,tol);
    EXPECT_NEAR((traj.End()-3)->SpeedZ(),0.0,tol);
    EXPECT_NEAR((traj.End()-3)->HeadingRate(),30.0/300.0,tol);
    EXPECT_NEAR((traj.End()-3)->Time(),63.1000,tol);
    
    // random point
    const double big_tol (1.9e-3);
    EXPECT_NEAR((traj.Begin()+233)->Pos()[0],-334.9798,tol);
    EXPECT_NEAR((traj.Begin()+233)->Pos()[1],85.4777,big_tol);
    EXPECT_NEAR((traj.Begin()+233)->Pos()[2],20.0,tol);
    //TODO wrap heading
    EXPECT_NEAR((traj.Begin()+233)->Heading(),4.7008,tol);
    EXPECT_NEAR((traj.Begin()+233)->SpeedXY(),30.0,tol);
    EXPECT_NEAR((traj.Begin()+233)->SpeedZ(),0.0,tol);
    EXPECT_NEAR((traj.Begin()+233)->HeadingRate(),30.0/300.0,tol);
    EXPECT_NEAR((traj.Begin()+233)->Time(),23.7000,tol);

}

// Run all the tests that were declared with TEST()
int32_t 
main(int32_t argc, char **argv){
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


