/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
#include "emergency_maneuver_library/open_loop_emergency_maneuver.h"
// Bring in gtest
#include <gtest/gtest.h>

using namespace ca;
using namespace emergency_maneuver_library;

TEST(EMERGENCY_MANEUVER_LIBRARY, OpenLoopEmergencyManeuverHighSpeed) {
    
    double high_speed_thresh = 20;
    double vel_z_max = 7.0;
    double acc_z_max = 0.9;
    double acc_z_res = 0.2 * acc_z_max;

    double roll_max = 0.5;
    double roll_max_res = roll_max/3.0;
    double roll_rate = -0.25;

    double head_rate_acc = 0.1;
    double head_rate_max = 0.3;
    double head_rate_max_res = 0.1;

    double acc_xy_max = -0.8;
    double time_res = 0.1;

    OpenLoopEmergencyManeuver olem(high_speed_thresh,acc_xy_max,vel_z_max, acc_z_max, acc_z_res, roll_max, roll_max_res, roll_rate, 
        head_rate_acc, head_rate_max, head_rate_max_res,time_res);

    StateFW init0(10.0,-20.0,-40.0,0.0,50.0,-6.0,0.0,0.0);//x y z h vxy vz vh time

    std::pair<Trajectory<StateFW>,Loiter> em0 (olem.GetHighSpeedEmergencyTrajectory(init0,high_speed_thresh,vel_z_max,roll_rate,roll_max,acc_xy_max,acc_z_max));
    Trajectory<StateFW> traj0 (em0.first);
    Loiter loit0 (em0.second);
    EXPECT_EQ(traj0.Size(),242);
    const double tol(1e-2);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[0], 10.0,tol);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[1],-20.0,tol);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[2],-40.0,tol);
    
    EXPECT_NEAR((traj0.Begin()+ 9)->Pos()[0],54.7090,tol);
    EXPECT_NEAR((traj0.Begin()+ 9)->Pos()[1],-19.5943,tol);
    EXPECT_NEAR((traj0.Begin()+ 9)->Pos()[2],-44.9950,tol);
    
    EXPECT_NEAR((traj0.Begin()+99)->Pos()[0],400.1321,0.2);
    EXPECT_NEAR((traj0.Begin()+99)->Pos()[1],167.5996,0.2);
    EXPECT_NEAR((traj0.Begin()+99)->Pos()[2],-54.8500,0.2);

    EXPECT_NEAR((traj0.Back()).Pos()[0],226.5129,1.2);
    EXPECT_NEAR((traj0.Back()).Pos()[1],559.6219,1.2);
    EXPECT_NEAR((traj0.Back()).Pos()[2],  7.8100,tol);
    EXPECT_NEAR((traj0.Back()).SpeedXY(),30.8000,tol);
    EXPECT_DOUBLE_EQ((traj0.Back()).SpeedZ(),0.0);
    EXPECT_NEAR((traj0.Back()).Head(),    3.1511,tol);
    EXPECT_NEAR((traj0.Back()).HeadRate(),0.1739,tol);
    EXPECT_NEAR((traj0.Back()).Time(),   24.1000,tol);
    
    EXPECT_NEAR(loit0.GetRadius(),177.0711,0.1);
    EXPECT_NEAR(loit0.GetSpeed(),30.8000,tol);
    EXPECT_NEAR(loit0.GetCenter()[0],228.2036,1.0);
    EXPECT_NEAR(loit0.GetCenter()[1],382.5589,1.0);
    EXPECT_TRUE(loit0.IsClockwise()==false);
    EXPECT_TRUE(loit0.IsHover()==false);

    /* second loiter */
    StateFW init1(-10.0,+20.0,+40.0,-2.0/3.0 * M_PI,50.0,2.0,0.0398,5.0);//x y z h vxy vz vh time
    roll_rate = 0.2; vel_z_max = 20.0; acc_z_max = -0.9;
    std::pair<Trajectory<StateFW>,Loiter> em1 (olem.GetHighSpeedEmergencyTrajectory(init1,high_speed_thresh,vel_z_max,roll_rate,roll_max,acc_xy_max,acc_z_max));
    Trajectory<StateFW> traj1 (em1.first);
    Loiter loit1 (em1.second);
    EXPECT_EQ(traj1.Size(),251);
    EXPECT_NEAR((traj1.Begin()   )->Pos()[0],-10.0,tol);
    EXPECT_NEAR((traj1.Begin()   )->Pos()[1], 20.0,tol);
    EXPECT_NEAR((traj1.Begin()   )->Pos()[2], 40.0,tol);
    EXPECT_NEAR((traj1.Begin()   )->Head(),-2.0/3.0*M_PI,tol);
    EXPECT_NEAR((traj1.Begin()   )->SpeedXY(),50.0,tol);
    EXPECT_NEAR((traj1.Begin()   )->SpeedZ(),2.0,tol);
    EXPECT_NEAR((traj1.Begin()   )->HeadRate(),0.0398,tol);
    EXPECT_NEAR((traj1.Begin()   )->Time(),5.0,tol);
    
    EXPECT_NEAR((traj1.Begin()+ 9)->Pos()[0],-31.8688,tol);
    EXPECT_NEAR((traj1.Begin()+ 9)->Pos()[1],-18.9984,tol);
    EXPECT_NEAR((traj1.Begin()+ 9)->Pos()[2], 41.3950,tol);
    EXPECT_NEAR((traj1.Begin()+ 9)->Head(),   -2.0766,tol);
    EXPECT_NEAR((traj1.Begin()+ 9)->SpeedXY(),49.3600,tol);
    EXPECT_NEAR((traj1.Begin()+ 9)->SpeedZ(),  1.1900,tol);
    EXPECT_NEAR((traj1.Begin()+ 9)->HeadRate(),-0.0040,tol);
    EXPECT_NEAR((traj1.Begin()+ 9)->Time(),    5.9000,tol);
    
    EXPECT_NEAR((traj1.Begin()+99)->Pos()[0],-335.4349,0.2);
    EXPECT_NEAR((traj1.Begin()+99)->Pos()[1],-271.6958,0.2);
    EXPECT_NEAR((traj1.Begin()+99)->Pos()[2],  15.2500,0.2);

    EXPECT_NEAR((traj1.Back()).Pos()[0],-626.8319,1.2);
    EXPECT_NEAR((traj1.Back()).Pos()[1],  55.4578,1.0);
    EXPECT_NEAR((traj1.Back()).Pos()[2], -74.5000,tol);
    EXPECT_NEAR((traj1.Back()).Head(),    -5.2474,tol);
    EXPECT_NEAR((traj1.Back()).SpeedXY(), 30.0800,tol);
    EXPECT_NEAR((traj1.Back()).SpeedZ(),  0.0000,tol);
    EXPECT_NEAR((traj1.Back()).HeadRate(),-0.1781,tol);
    EXPECT_NEAR((traj1.Back()).Time(),    30.0000,tol);
    EXPECT_NEAR((traj1.Back()).Roll(), roll_max,tol);
    
    EXPECT_NEAR(loit1.GetRadius(),168.8892,1.0);
    EXPECT_NEAR(loit1.GetSpeed(),  30.0800,tol);
    EXPECT_NEAR(loit1.GetCenter()[0],-481.5569,0.5);
    EXPECT_NEAR(loit1.GetCenter()[1], -30.7327,0.5);
    EXPECT_NEAR(loit1.GetCenter()[2], -74.5000,0.5);
    EXPECT_TRUE(loit1.IsClockwise());
    EXPECT_TRUE(loit0.IsHover()==false);
}

TEST(EMERGENCY_MANEUVER_LIBRARY, OpenLoopEmergencyManeuverSlowSpeed) {
    double high_speed_thresh= 20.0;
    double vel_z_max = 3.0;
    double acc_z_max = 0.3;
    double acc_z_res = 0.2 * acc_z_max;

    double roll_max = 0.5;
    double roll_max_res = roll_max/3.0;
    double roll_rate = -0.25;
    
    double head_rate_acc = -0.01;
    double head_rate_max = 0.1;
    double head_rate_max_res = 0.05;

    double acc_xy_max = -0.8;
    double time_res = 0.1;

    OpenLoopEmergencyManeuver olem(high_speed_thresh, acc_xy_max,vel_z_max, acc_z_max, acc_z_res, roll_max, roll_max_res, roll_rate, 
        head_rate_acc,head_rate_max,head_rate_max_res, time_res);

    StateFW init0(-10.0,-20.0,5.0,2.0/3.0*M_PI,20.0,-1.0,-0.05,8.0);//x y z h vxy vz vh time
    const double vel_xy_min (0.0);//reach hover
    std::pair<Trajectory<StateFW>,Loiter> em0 (olem.GetSlowSpeedEmergencyTrajectory(init0,vel_xy_min,vel_z_max,head_rate_acc,head_rate_max,acc_xy_max,acc_z_max));
    Trajectory<StateFW> traj0 (em0.first);
    Loiter loit0 (em0.second);
    EXPECT_EQ(traj0.Size(),252);
    const double tol(1e-2);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[0],-10.0,tol);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[1],-20.0,tol);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[2], 5.0,tol);
    
    EXPECT_NEAR((traj0.Begin()+ 9)->Pos()[0],-18.4427,tol);
    EXPECT_NEAR((traj0.Begin()+ 9)->Pos()[1],-4.4317,tol);
    EXPECT_NEAR((traj0.Begin()+ 9)->Pos()[2], 4.2350,tol);
    
    EXPECT_NEAR((traj0.Begin()+99)->Pos()[0],-35.1914,0.2);
    EXPECT_NEAR((traj0.Begin()+99)->Pos()[1],131.9631,0.2);
    EXPECT_NEAR((traj0.Begin()+99)->Pos()[2],  9.9500,0.2);

    EXPECT_NEAR((traj0.Back()).Pos()[0], 29.4671,1.2);
    EXPECT_NEAR((traj0.Back()).Pos()[1],188.8228,1.2);
    EXPECT_NEAR((traj0.Back()).Pos()[2], 38.6830,0.5);
    EXPECT_NEAR((traj0.Back()).Head(),   -0.2931,tol);
    EXPECT_NEAR((traj0.Back()).SpeedXY(), 0.0,tol);
    EXPECT_NEAR((traj0.Back()).SpeedZ(),  0.0,tol);
    EXPECT_NEAR((traj0.Back()).HeadRate(),-0.1000,tol);
    EXPECT_NEAR((traj0.Back()).Time(),   33.1000,tol);
    
    EXPECT_DOUBLE_EQ(loit0.GetRadius(),0.0);
    EXPECT_DOUBLE_EQ(loit0.GetSpeed() ,0.0);
    EXPECT_NEAR(loit0.GetCenter()[0],29.4671,tol);
    EXPECT_NEAR(loit0.GetCenter()[1],188.8228,tol);
    EXPECT_TRUE(loit0.IsClockwise());
    EXPECT_TRUE(loit0.IsHover());
}

TEST(EMERGENCY_MANEUVER_LIBRARY, OpenLoopEmergencyManeuverSlowSpeedSet) {
    
    double high_speed_thresh= 20.0;
    double vel_z_max = 10.0;
    double acc_z_max = 0.5;
    double acc_z_max_res = 0.1;

    // not needed for this test... only slow
    double roll_max = 0.0;
    double roll_max_res = 0.0;
    double roll_rate = 0.0;
    
    double head_rate_acc = 0.03;
    double head_rate_max = 0.2;
    double head_rate_max_res = 0.05;

    double acc_xy_max = -0.5;
    double time_res = 0.2;

    OpenLoopEmergencyManeuver olem(high_speed_thresh, acc_xy_max, vel_z_max, acc_z_max, acc_z_max_res, roll_max, roll_max_res, roll_rate, 
        head_rate_acc,head_rate_max,head_rate_max_res, time_res);

    StateFW init0(25.0,100.0,-20.0,M_PI,15.0,2.0,0.04,13);//x y z h vxy vz vh time
    std::vector<std::pair<Trajectory<StateFW>,Loiter> > emset0 (olem.GetSlowSpeedEmergencyTrajectories(init0));
    EXPECT_EQ(emset0.size(),90);
    /* investigate 2 of them*/
    Trajectory<StateFW> traj0 = emset0[9].first;
    EXPECT_EQ(traj0.Size(),153);
    const double tol(1e-2);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[0],25.0,tol);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[1],100.0,tol);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[2],-20,tol);
    
    EXPECT_NEAR((traj0.Begin()+99)->Pos()[0],-159.3768,0.2);
    EXPECT_NEAR((traj0.Begin()+99)->Pos()[1],  35.8725,0.2);
    EXPECT_NEAR((traj0.Begin()+99)->Pos()[2], -57.5045,0.2);

    EXPECT_NEAR((traj0.Back()).Pos()[0],-175.0880,1.2);
    EXPECT_NEAR((traj0.Back()).Pos()[1],  14.6624,1.2);
    EXPECT_NEAR((traj0.Back()).Pos()[2], -79.2080,0.5);
    EXPECT_NEAR((traj0.Back()).Head(),     4.3576,tol);
    EXPECT_NEAR((traj0.Back()).SpeedXY(),  0.0,tol);
    EXPECT_DOUBLE_EQ((traj0.Back()).SpeedZ(),   0.0);
    EXPECT_NEAR((traj0.Back()).HeadRate(), 0.0400,tol);
    EXPECT_NEAR((traj0.Back()).Time(),    43.4000,tol);
    /*second traj*/
    Trajectory<StateFW> traj1 = emset0[74].first;
    EXPECT_EQ(traj1.Size(),153);
    EXPECT_NEAR((traj1.Begin()   )->Pos()[0],25.0,tol);
    EXPECT_NEAR((traj1.Begin()   )->Pos()[1],100.0,tol);
    EXPECT_NEAR((traj1.Begin()   )->Pos()[2],-20,tol);
    
    EXPECT_NEAR((traj1.Begin()+99)->Pos()[0],-159.4933,0.2);
    EXPECT_NEAR((traj1.Begin()+99)->Pos()[1], 157.0300,0.2);
    EXPECT_NEAR((traj1.Begin()+99)->Pos()[2],  77.6077,0.2);

    EXPECT_NEAR((traj1.Back()).Pos()[0],-172.8084,1.2);
    EXPECT_NEAR((traj1.Back()).Pos()[1], 179.7397,1.2);
    EXPECT_NEAR((traj1.Back()).Pos()[2],  99.4080,0.5);
    EXPECT_NEAR((traj1.Back()).Head(),     1.7476,tol);
    EXPECT_DOUBLE_EQ((traj1.Back()).SpeedXY(),  0.0);
    EXPECT_DOUBLE_EQ((traj1.Back()).SpeedZ(),   0.0);
    EXPECT_NEAR((traj1.Back()).HeadRate(),-0.0500,tol);
    EXPECT_NEAR((traj1.Back()).Time(),    43.4000,tol);
}

TEST(EMERGENCY_MANEUVER_LIBRARY, OpenLoopEmergencyManeuverHighSpeedSet) {
    double high_speed_thresh= 20.0;
    double vel_z_max = 10.0;
    double acc_z_max = 0.5;
    double acc_z_max_res = 0.1;

    double roll_max = 0.5;
    double roll_max_res = 0.1;
    double roll_rate = 0.2;
    
    // not needed for this test... only for slow
    double head_rate_acc = 0.0;
    double head_rate_max = 0.0;
    double head_rate_max_res = 0.00;

    double acc_xy_max = -0.5;
    double time_res = 0.2;

    OpenLoopEmergencyManeuver olem(high_speed_thresh, acc_xy_max, vel_z_max, acc_z_max, acc_z_max_res, roll_max, roll_max_res, roll_rate, 
        head_rate_acc,head_rate_max,head_rate_max_res, time_res);

    double roll0 = 0.05;
    double head_rate0 = -(tan(roll0)*9.80665)/44.5;
    std::cout << "head_rate0: "<< head_rate0 << std::endl;
    StateFW init0(25.0,100.0,-20.0,-M_PI,44.5,2.0,head_rate0,13);//x y z h vxy vz vh time
    std::vector<std::pair<Trajectory<StateFW>,Loiter> > emset0 (olem.GetHighSpeedEmergencyTrajectories(init0));
    EXPECT_EQ(emset0.size(),100);
    /* investigate 2 of them*/
    Trajectory<StateFW> traj0 = emset0[9].first;
    EXPECT_EQ(traj0.Size(),120);
    const double tol(1e-2);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[0],25.0,tol);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[1],100.0,tol);
    EXPECT_NEAR((traj0.Begin()   )->Pos()[2],-20,tol);
    
    EXPECT_NEAR((traj0.Begin()+19)->Pos()[0],-139.0646,0.2);
    EXPECT_NEAR((traj0.Begin()+19)->Pos()[1],  82.9477,0.2);
    EXPECT_NEAR((traj0.Begin()+19)->Pos()[2], -16.2000,0.2);

    EXPECT_NEAR((traj0.Back()).Pos()[0],-130.3662,1.5);
    EXPECT_NEAR((traj0.Back()).Pos()[1],-441.0412,1.2);
    EXPECT_NEAR((traj0.Back()).Pos()[2], -64.7100,0.5);
    EXPECT_NEAR((traj0.Back()).Head(),     0.0110,tol);// what...more than 2 pi... check that
    EXPECT_NEAR((traj0.Back()).SpeedXY(), 32.7000,tol);
    EXPECT_NEAR((traj0.Back()).SpeedZ(),   0.0000,tol);
    EXPECT_NEAR((traj0.Back()).HeadRate(),0.1638 ,tol);
    EXPECT_NEAR((traj0.Back()).Time(),    36.8000,tol);
    /*second traj*/
    Trajectory<StateFW> traj1 = emset0[74].first;
    EXPECT_EQ(traj1.Size(),183);
    EXPECT_NEAR((traj1.Begin()   )->Pos()[0],25.0,tol);
    EXPECT_NEAR((traj1.Begin()   )->Pos()[1],100.0,tol);
    EXPECT_NEAR((traj1.Begin()   )->Pos()[2],-20,tol);
    
    EXPECT_NEAR((traj1.Begin()+19)->Pos()[0],-139.2621,0.2);
    EXPECT_NEAR((traj1.Begin()+19)->Pos()[1], 117.9526,0.2);
    EXPECT_NEAR((traj1.Begin()+19)->Pos()[2], -10.1200,0.2);

    EXPECT_NEAR((traj1.Back()).Pos()[0],-245.9383,1.3);
    EXPECT_NEAR((traj1.Back()).Pos()[1], 892.8290,1.2);
    EXPECT_NEAR((traj1.Back()).Pos()[2], 111.8080,0.5);
    EXPECT_NEAR((traj1.Back()).Head(),    -6.2969,tol);// TODO, need to wrap heading what...more than 2 pi... check that
    EXPECT_NEAR((traj1.Back()).SpeedXY(), 26.4000,tol);
    EXPECT_DOUBLE_EQ((traj1.Back()).SpeedZ(),   0.0);
    EXPECT_NEAR((traj1.Back()).HeadRate(),-0.1149,tol);
    EXPECT_NEAR((traj1.Back()).Time(),    49.4000,tol);
}

// Run all the tests that were declared with TEST()
int32_t 
main(int32_t argc, char **argv){
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
