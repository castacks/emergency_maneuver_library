/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
#include "emergency_maneuver_library/trajectory.h"
// Bring in gtest
#include <gtest/gtest.h>

using namespace ca;
using namespace emergency_maneuver_library;

TEST(EMERGENCY_MANEUVER_LIBRARY, State) {
	/*default constructor*/
	State state0;
	
	EXPECT_DOUBLE_EQ(state0.Pos()[0],0.0);
	EXPECT_DOUBLE_EQ(state0.Pos()[1],0.0);
	EXPECT_DOUBLE_EQ(state0.Pos()[2],0.0);
	
	EXPECT_DOUBLE_EQ(state0.BodyVel()[0],0.0);
	EXPECT_DOUBLE_EQ(state0.BodyVel()[1],0.0);
	EXPECT_DOUBLE_EQ(state0.BodyVel()[2],0.0);
	
	EXPECT_DOUBLE_EQ(state0.BodyAcc()[0],0.0);
	EXPECT_DOUBLE_EQ(state0.BodyAcc()[1],0.0);
	EXPECT_DOUBLE_EQ(state0.BodyAcc()[2],0.0);
	
	EXPECT_DOUBLE_EQ(state0.Orient()[0],0.0);
	EXPECT_DOUBLE_EQ(state0.Orient()[1],0.0);
	EXPECT_DOUBLE_EQ(state0.Orient()[2],0.0);
	
	EXPECT_DOUBLE_EQ(state0.OrientVel()[0],0.0);
	EXPECT_DOUBLE_EQ(state0.OrientVel()[1],0.0);
	EXPECT_DOUBLE_EQ(state0.OrientVel()[2],0.0);
	
	EXPECT_DOUBLE_EQ(state0.OrientAcc()[0],0.0);
	EXPECT_DOUBLE_EQ(state0.OrientAcc()[1],0.0);
	EXPECT_DOUBLE_EQ(state0.OrientAcc()[2],0.0);

	EXPECT_DOUBLE_EQ(state0.Time(),0.0);

	/*initialize list*/
	State state1 {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
	EXPECT_DOUBLE_EQ(state1.Pos()[0],0.0);
	EXPECT_DOUBLE_EQ(state1.Pos()[1],1.0);
	EXPECT_DOUBLE_EQ(state1.Pos()[2],2.0);
	
	EXPECT_DOUBLE_EQ(state1.BodyVel()[0],3.0);
	EXPECT_DOUBLE_EQ(state1.BodyVel()[1],4.0);
	EXPECT_DOUBLE_EQ(state1.BodyVel()[2],5.0);
	
	EXPECT_DOUBLE_EQ(state1.BodyAcc()[0],6.0);
	EXPECT_DOUBLE_EQ(state1.BodyAcc()[1],7.0);
	EXPECT_DOUBLE_EQ(state1.BodyAcc()[2],8.0);
	
	EXPECT_DOUBLE_EQ(state1.Orient()[0], 9.0);
	EXPECT_DOUBLE_EQ(state1.Orient()[1],10.0);
	EXPECT_DOUBLE_EQ(state1.Orient()[2],11.0);
	
	EXPECT_DOUBLE_EQ(state1.OrientVel()[0],12.0);
	EXPECT_DOUBLE_EQ(state1.OrientVel()[1],13.0);
	EXPECT_DOUBLE_EQ(state1.OrientVel()[2],14.0);
	
	EXPECT_DOUBLE_EQ(state1.OrientAcc()[0],15.0);
	EXPECT_DOUBLE_EQ(state1.OrientAcc()[1],16.0);
	EXPECT_DOUBLE_EQ(state1.OrientAcc()[2],17.0);

	EXPECT_DOUBLE_EQ(state1.Time(),18.0);

	/*nested initialize lists*/
	State state2 { {0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {9, 10, 11}, {12, 13, 14}, {15, 16, 17}, {18} };
	EXPECT_DOUBLE_EQ(state2.Pos()[0],0.0);
	EXPECT_DOUBLE_EQ(state2.Pos()[1],1.0);
	EXPECT_DOUBLE_EQ(state2.Pos()[2],2.0);
	
	EXPECT_DOUBLE_EQ(state2.BodyVel()[0],3.0);
	EXPECT_DOUBLE_EQ(state2.BodyVel()[1],4.0);
	EXPECT_DOUBLE_EQ(state2.BodyVel()[2],5.0);
	
	EXPECT_DOUBLE_EQ(state2.BodyAcc()[0],6.0);
	EXPECT_DOUBLE_EQ(state2.BodyAcc()[1],7.0);
	EXPECT_DOUBLE_EQ(state2.BodyAcc()[2],8.0);
	
	EXPECT_DOUBLE_EQ(state2.Orient()[0], 9.0);
	EXPECT_DOUBLE_EQ(state2.Orient()[1],10.0);
	EXPECT_DOUBLE_EQ(state2.Orient()[2],11.0);
	
	EXPECT_DOUBLE_EQ(state2.OrientVel()[0],12.0);
	EXPECT_DOUBLE_EQ(state2.OrientVel()[1],13.0);
	EXPECT_DOUBLE_EQ(state2.OrientVel()[2],14.0);
	
	EXPECT_DOUBLE_EQ(state2.OrientAcc()[0],15.0);
	EXPECT_DOUBLE_EQ(state2.OrientAcc()[1],16.0);
	EXPECT_DOUBLE_EQ(state2.OrientAcc()[2],17.0);

	EXPECT_DOUBLE_EQ(state2.Time(),18.0);

	/*setter and getter*/
	Vector3D pos0(1.2,2.3,3.4);
	Vector3D vel0(4.2,5.3,6.4);
	Vector3D acc0(7.2,8.3,9.4);
	Vector3D orient0(-1.2,-2.3,-3.4);
	Vector3D orient_vel0(-4.2,-5.3,-6.4);
	Vector3D orient_acc0(-7.2,-8.3,-9.4);
	double time0(+10.4);
	
	state2.Pos(pos0);
	EXPECT_TRUE(state2.Pos()==pos0);
	state2.BodyVel(vel0);
	EXPECT_TRUE(state2.BodyVel()==vel0);
	state2.BodyAcc(acc0);
	EXPECT_TRUE(state2.BodyAcc()==acc0);
	state2.Orient(orient0);
	EXPECT_TRUE(state2.Orient()==orient0);
	state2.OrientVel(orient_vel0);
	EXPECT_TRUE(state2.OrientVel()==orient_vel0);
	state2.OrientAcc(orient_acc0);
	EXPECT_TRUE(state2.OrientAcc()==orient_acc0);
	state2.Time(time0);
	EXPECT_TRUE(state2.Time()==time0);
}

TEST(EMERGENCY_MANEUVER_LIBRARY, StateFW) {
	/*default constructor*/
	StateFW state0;
	
	EXPECT_DOUBLE_EQ(state0.Pos()[0],0.0);
	EXPECT_DOUBLE_EQ(state0.Pos()[1],0.0);
	EXPECT_DOUBLE_EQ(state0.Pos()[2],0.0);
	
	EXPECT_DOUBLE_EQ(state0.SpeedXY(),0.0);
	EXPECT_DOUBLE_EQ(state0.SpeedZ(),0.0);
	
	EXPECT_DOUBLE_EQ(state0.Heading(),0.0);
	EXPECT_DOUBLE_EQ(state0.HeadingRate(),0.0);

	EXPECT_DOUBLE_EQ(state0.Time(),0.0);

	/*initialize list*/
	StateFW state1 {0, 1, 2, 3, 4, 5, 6, 7};
	EXPECT_DOUBLE_EQ(state1.Pos()[0],0.0);
	EXPECT_DOUBLE_EQ(state1.Pos()[1],1.0);
	EXPECT_DOUBLE_EQ(state1.Pos()[2],2.0);
	EXPECT_DOUBLE_EQ(state1.Heading(),3.0);
	EXPECT_DOUBLE_EQ(state1.SpeedXY(),4.0);
	EXPECT_DOUBLE_EQ(state1.SpeedZ(),5.0);
	EXPECT_DOUBLE_EQ(state1.HeadingRate(),6.0);
	EXPECT_DOUBLE_EQ(state1.Time(),7.0);
	
    /*setter and getter*/
	Vector3D pos0(1.2,2.3,3.4);
	double head (4.2);
	double speedxy (5.3);
    double speedz  (6.4);
	double headrate (7.2);
	double time (10.4);
	
	state1.Pos(pos0);
	EXPECT_TRUE(state1.Pos()==pos0);
	state1.SpeedXY(speedxy);
	EXPECT_TRUE(state1.SpeedXY()==speedxy);
	state1.SpeedZ(speedz);
	EXPECT_TRUE(state1.SpeedZ()==speedz);
	state1.Heading(head);
	EXPECT_TRUE(state1.Heading()==head);
	state1.HeadingRate(headrate);
	EXPECT_TRUE(state1.HeadingRate()==headrate);
	state1.Time(time);
	EXPECT_TRUE(state1.Time()==time);

}

TEST(EMERGENCY_MANEUVER_LIBRARY, Trajectory) {
	Trajectory<State> traj0;
	EXPECT_EQ(traj0.Size(),0);
	EXPECT_EQ(traj0.GetPositions().size(),0);
	
	/*some states*/
	State state0 {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
	State state1 {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	/*append*/
	traj0.Append(state0);
	EXPECT_EQ(traj0.Size(),1);
	EXPECT_EQ(traj0.GetPositions().size(),1);
	/*erase*/
	Trajectory<State>::const_iterator c_it (traj0.Begin());
	traj0.Erase(c_it);
	EXPECT_EQ(traj0.Size(),0);
	EXPECT_EQ(traj0.GetPositions().size(),0);
	/*append*/
	traj0.Append(state0);
	traj0.Append(state0);
	EXPECT_EQ(traj0.Size(),2);
	EXPECT_EQ(traj0.GetPositions().size(),2);

	/*front & back*/
	EXPECT_TRUE(traj0.Front() == state0);
	EXPECT_TRUE(traj0.Back() == state0);
	
	/*begin & end*/
	EXPECT_TRUE(*traj0.Begin() == state0);
	EXPECT_TRUE(*(traj0.End()-1) == state0);
	EXPECT_EQ(std::distance(traj0.Begin(),traj0.End()), traj0.Size());

	/*insert*/
	traj0.Insert(traj0.Begin()+1,state1);
	EXPECT_TRUE(traj0.Front() == state0);
	EXPECT_TRUE(traj0.Back() == state0);
	EXPECT_TRUE(*traj0.Begin() == state0);
	EXPECT_TRUE(*(traj0.Begin()+1) == state1);
	EXPECT_TRUE(*(traj0.End()-2) == state1);
	EXPECT_TRUE(*(traj0.End()-1) == state0);
	/*insert*/
	traj0.Insert(traj0.Begin(),state1);
	EXPECT_TRUE(*(traj0.Begin()+0) == state1);
	EXPECT_TRUE(*(traj0.Begin()+1) == state0);
	EXPECT_TRUE(*(traj0.Begin()+2) == state1);
	EXPECT_TRUE(*(traj0.Begin()+3) == state0);
	/* GetPositions */
	std::vector<Vector3D> pos (traj0.GetPositions());
	EXPECT_EQ(pos.size(), traj0.Size());
	EXPECT_EQ(pos[0], traj0.Front().Pos());
	EXPECT_EQ(pos[0], state1.Pos());
	EXPECT_EQ(pos[1], state0.Pos());
	EXPECT_EQ(pos[2], state1.Pos());
	EXPECT_EQ(pos[3], state0.Pos());
	EXPECT_EQ(pos[3], traj0.Back().Pos());
	// not true test
	EXPECT_NE(pos[0], state0.Pos());
	EXPECT_NE(pos[1], state1.Pos());
	EXPECT_NE(pos[2], state0.Pos());
	EXPECT_NE(pos[3], state1.Pos());

	/* erase */
	traj0.Erase(traj0.Begin()+2);
	EXPECT_EQ(traj0.Size(),3);
	EXPECT_TRUE(*(traj0.Begin()+0) == state1);
	EXPECT_TRUE(*(traj0.Begin()+1) == state0);
	EXPECT_TRUE(*(traj0.Begin()+2) == state0);
	EXPECT_EQ(traj0.GetPositions().size(),3);
}

TEST(EMERGENCY_MANEUVER_LIBRARY, Trajectory2) {
	Trajectory<StateFW> traj0;
	EXPECT_EQ(traj0.Size(),0);
	EXPECT_EQ(traj0.GetPositions().size(),0);
    StateFW st0 {0, 0, 0, 0, 0, 0, 0, 0,};
    StateFW st1 {1, 0, 0, 0, 0, 0, 0, 1};
    StateFW st2 {2, 0, 0, 0, 0, 0, 0, 2};
    StateFW st3 {4, 0, 0, 0, 0, 0, 0, 3};
    StateFW st4 {8, 0, 0, 0, 0, 0, 0, 4};
    traj0.Append(st0);
    traj0.Append(st1);
    traj0.Append(st2);
    traj0.Append(st3);
    traj0.Append(st4);
	EXPECT_EQ(traj0.Size(),5);


	Trajectory<StateFW> traj1;
	EXPECT_EQ(traj1.Size(),0);
	EXPECT_EQ(traj1.GetPositions().size(),0);
    StateFW st5 {16,  0, 0, 0, 0, 0, 0, 5};
    StateFW st6 {32,  0, 0, 0, 0, 0, 0, 6};
    StateFW st7 {64,  0, 0, 0, 0, 0, 0, 7};
    StateFW st8 {128, 0, 0, 0, 0, 0, 0, 8};
    StateFW st9 {256, 0, 0, 0, 0, 0, 0, 9};
    traj1.Append(st5);
    traj1.Append(st6);
    traj1.Append(st7);
    traj1.Append(st8);
    traj1.Append(st9);
	EXPECT_EQ(traj1.Size(),5);

    traj0.Append(traj1);
	EXPECT_TRUE(*(traj0.Begin()+0) == st0);
	EXPECT_TRUE(*(traj0.Begin()+1) == st1);
	EXPECT_TRUE(*(traj0.Begin()+2) == st2);
	EXPECT_TRUE(*(traj0.Begin()+3) == st3);
	EXPECT_TRUE(*(traj0.Begin()+4) == st4);
	EXPECT_TRUE(*(traj0.Begin()+5) == st5);
	EXPECT_TRUE(*(traj0.Begin()+6) == st6);
	EXPECT_TRUE(*(traj0.Begin()+7) == st7);
	EXPECT_TRUE(*(traj0.Begin()+8) == st8);
	EXPECT_TRUE(*(traj0.Begin()+9) == st9);
	EXPECT_EQ(traj0.Size(),10);
}

// Run all the tests that were declared with TEST()
int32_t 
main(int32_t argc, char **argv){
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


