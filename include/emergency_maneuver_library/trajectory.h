/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/******************************************************************************
 * Filename: trajectory.h
 * Description:
 *
 * Version: 1.0
 * Created: Sep 16 2014 16:42:54
 * Last modified: Nov 23 2015 07:48:03 PM
 * Revision: None
 *
 * Author:  althoff
 * e-mail:  althoff@andrew.cmu.edu
 *
 * Revision history
 * Date Author Remarks
 * Sep 16 2014 althoff File created.
 *
 ******************************************************************************/
#ifndef CA_EMERGENCY_MANEUVER_TRAJECTORY_LIBRARY_H_
#define CA_EMERGENCY_MANEUVER_TRAJECTORY_LIBRARY_H_

#include <Eigen/Core>
#include <boost/assert.hpp>

#include <iostream> // cout

#include "exception/exception.h"

namespace ca {
namespace emergency_maneuver_library {
constexpr static double g = 9.80665;

typedef Eigen::Matrix<double, 3, 1> Vector3D;

/**
 * \brief contains all information pertinent to a state for trajectory generation
 */
class State {
 public:

  /**
   * \brief Default constructor
   */
  State()
      : pos_(0.0, 0.0, 0.0),
        body_vel_(0.0, 0.0, 0.0),
        body_acc_(0.0, 0.0, 0.0),
        orient_(0.0, 0.0, 0.0),
        orient_vel_(0.0, 0.0, 0.0),
        orient_acc_(0.0, 0.0, 0.0),
        time_(0.0) {
  }
  ;

  /**
   * \brief Initialization constructor
   * @param pos position (m)
   * @param body_vel linear velocity (m/s)
   * @param body_acc linear acceleration (m/s^2)
   * @param orient orientation (rad)
   * @param orient_vel angular velocity (rad/s)
   * @param orient_acc angular acceleration (rad/s^2)
   * @param time time stamp (s)
   */
  State(Vector3D pos, Vector3D body_vel, Vector3D body_acc, Vector3D orient,
        Vector3D orient_vel, Vector3D orient_acc, double time)
      : pos_(pos),
        body_vel_(body_vel),
        body_acc_(body_acc),
        orient_(orient),
        orient_vel_(orient_vel),
        orient_acc_(orient_acc),
        time_(time) {
  }
  ;
  // I know this is somehow bullshit, since it requires fixed size... but i wanted to try it
  typedef std::initializer_list<double> list_type;
  typedef std::initializer_list<list_type> llist_type;

  /**
   * \brief Constructor with list of float values
   * @param l
   */
  State(list_type l) {
    BOOST_ASSERT_MSG(l.size() != 19, "initializer list has wrong size");

    pos_ = Vector3D(*l.begin(), *(l.begin() + 1), *(l.begin() + 2));
    body_vel_ = Vector3D(*(l.begin() + 3), *(l.begin() + 4), *(l.begin() + 5));
    body_acc_ = Vector3D(*(l.begin() + 6), *(l.begin() + 7), *(l.begin() + 8));
    orient_ = Vector3D(*(l.begin() + 9), *(l.begin() + 10), *(l.begin() + 11));
    orient_vel_ = Vector3D(*(l.begin() + 12), *(l.begin() + 13),
                           *(l.begin() + 14));
    orient_acc_ = Vector3D(*(l.begin() + 15), *(l.begin() + 16),
                           *(l.begin() + 17));
    time_ = *(l.begin() + 18);
  }

  /**
   * \brief Constructor with list of lists of float values
   * @param l
   */
  State(llist_type l) {
    BOOST_ASSERT_MSG(l.size() != 7, "initializer list has wrong size");
    llist_type::const_iterator it_ll(l.begin());
    list_type::const_iterator it_l(it_ll->begin());

    BOOST_ASSERT_MSG(it_l->size() != 3, "initializer list has wrong size");
    pos_ = Vector3D(*it_l, *(it_l + 1), *(it_l + 2));
    it_l = (it_ll + 1)->begin();

    BOOST_ASSERT_MSG(it_l->size() != 3, "initializer list has wrong size");
    body_vel_ = Vector3D(*it_l, *(it_l + 1), *(it_l + 2));
    it_l = (it_ll + 2)->begin();

    BOOST_ASSERT_MSG(it_l->size() != 3, "initializer list has wrong size");
    body_acc_ = Vector3D(*it_l, *(it_l + 1), *(it_l + 2));
    it_l = (it_ll + 3)->begin();

    BOOST_ASSERT_MSG(it_l->size() != 3, "initializer list has wrong size");
    orient_ = Vector3D(*it_l, *(it_l + 1), *(it_l + 2));
    it_l = (it_ll + 4)->begin();

    BOOST_ASSERT_MSG(it_l->size() != 3, "initializer list has wrong size");
    orient_vel_ = Vector3D(*it_l, *(it_l + 1), *(it_l + 2));
    it_l = (it_ll + 5)->begin();

    BOOST_ASSERT_MSG(it_l->size() != 3, "initializer list has wrong size");
    orient_acc_ = Vector3D(*it_l, *(it_l + 1), *(it_l + 2));
    it_l = (it_ll + 6)->begin();

    BOOST_ASSERT_MSG(it_l->size() != 1, "initializer list has wrong size");
    time_ = *it_l;
  }
  ;

  /**
   * \brief Compares with other State for equality
   * @param state refernce to the other state
   * @return true if state members are equal (bit wise)
   */
  bool Equal(const State& state) const {
    // Do we need something like < 1e-9?
    if (state.Time() != this->Time() || state.Pos() != this->Pos()
        || state.BodyVel() != this->BodyVel()
        || state.BodyAcc() != this->BodyAcc()
        || state.Orient() != this->Orient()
        || state.OrientVel() != this->OrientVel()
        || state.OrientAcc() != this->OrientAcc()) {

      return false;
    }
    return true;
  }

  /**
   * \brief Equality operator with another State
   * @param state the other state
   * @return true if they are equal
   */
  bool operator ==(const State& state) const {
    return Equal(state);
  }

  /*Getter*/
  /**
   * \brief Returns the position of the state
   * @return in meters
   */
  Vector3D Pos(void) const {
    return pos_;
  }
  /**
   * \brief Returns the linear velocity of the state
   * @return in m/s
   */
  Vector3D BodyVel(void) const {
    return body_vel_;
  }
  /**
   * \brief Returns the linear acceleration of the state
   * @return in m/s^2
   */
  Vector3D BodyAcc(void) const {
    return body_acc_;
  }
  /**
   * \brief Returns the orientation of the state
   * @return roll, pitch, and yaw components in radians
   */
  Vector3D Orient(void) const {
    return orient_;
  }
  /**
   * \brief Returns the angular velocity of the state
   * @return in rad/s
   */
  Vector3D OrientVel(void) const {
    return orient_vel_;
  }
  /**
   * \brief Returns the angular acceleration of the state
   * @return in rad/s^2
   */
  Vector3D OrientAcc(void) const {
    return orient_acc_;
  }
  /**
   * \brief Returns the timestamp of the state
   * @return in seconds
   */
  double Time(void) const {
    return time_;
  }
  /*Setter*/
  /**
   * \brief Set the position of the state
   * @param pos vector in meters
   */
  void Pos(const Vector3D &pos) {
    pos_ = pos;
  }
  /**
   * \brief Set the linear velocity of the state
   * @param vel vector in m/s
   */
  void BodyVel(const Vector3D &vel) {
    body_vel_ = vel;
  }
  /**
   * \brief Set the linear acceleration of the state
   * @param acc vector in m/s^2
   */
  void BodyAcc(const Vector3D &acc) {
    body_acc_ = acc;
  }
  /**
   * \brief Set the orientation of the state
   * @param rpy roll, pitch, and yaw components in radians
   */
  void Orient(const Vector3D &rpy) {
    orient_ = rpy;
  }
  /**
   * \brief Set the angular velocity of the state
   * @param rpy_vel
   */
  void OrientVel(const Vector3D &rpy_vel) {
    orient_vel_ = rpy_vel;
  }
  /**
   * \brief Set the angular acceleration of the state
   * @param rpy_acc
   */
  void OrientAcc(const Vector3D & rpy_acc) {
    orient_acc_ = rpy_acc;
  }
  /**
   * \brief Set the timestamp of the state
   * @param time in seconds
   */
  void Time(double time) {
    time_ = time;
  }
 private:
  // ToDo! frame in which state exists.
  std::string frame; /**< \todo frame in which state exists. */
  Vector3D pos_;
  Vector3D body_vel_;
  Vector3D body_acc_;
  Vector3D orient_;
  Vector3D orient_vel_;
  Vector3D orient_acc_;
  double time_;
};

/**
 * \brief Encompasses trajectory information
 */
template<class State>
class Trajectory {
 public:
  typedef std::vector<State> Traj;
  typedef typename Traj::const_iterator const_iterator;
  typedef typename Traj::iterator iterator;
  //TODO check this, needed for std::back_inserter
  typedef typename Traj::value_type value_type;
  typedef typename Traj::const_reference const_reference;

  /**
   * \brief Default constructor
   */
  Trajectory()
      : pos_traj_valid_(false) {
  }
  ;

  /* all const methods */
  /**
   * \brief Retrieve the number of states in the trajectory
   * @return
   */
  size_t Size() const {
    return traj_.size();
  }

  /**
   * \brief Retrieve the first state in the trajectory
   * @return copy of the state
   */
  State Front(void) const {
    return traj_.front();
  }
  /**
   * \brief Retrieve the last state in the trajectory
   * @return copy of the state
   */
  State Back(void) const {
    return traj_.back();
  }

  /**
   * \brief Retrieve iterator at the front of the trajectory
   * @return the iterator
   */
  const_iterator Begin() const {
    return traj_.cbegin();
  }
  /**
   * \brief Retrieve iterator at the back of the trajectory
   * @return the iterator
   */
  const_iterator End() const {
    return traj_.cend();
  }

  /**
   * \brief Retrieve iterator at the front of the trajectory
   * @return the iterator
   */
  // so that range based for loops works
  const_iterator begin() const {
    return Begin();
  }
  /**
   * \brief Retrieve iterator at the back of the trajectory
   * @return the iterator
   */
  const_iterator end() const {
    return End();
  }

  /**
   * \brief Return the position components of the states along the trajectory
   * @return a list of positions
   */
  std::vector<Vector3D> GetPositions() {
    if (pos_traj_valid_ == false) {
      GeneratePosTraj();
    }
    return pos_traj_;
  }
  ;

  /**
   * \brief Set the timestamp of a state in the trajectory
   * @param idx the index of the state
   * @param time the time stamp
   */
  //not const, but does not change pos entries, so no change of pos_traj_valid flag
  void SetTime(size_t idx, double time) {
    traj_[idx].Time(time);
  }

  /* all non const methods */
  /* each non const methods makes the pos_traj_ invalid! */
  // TODO does not guarantee that somebody access this data later....
  /**
   * \brief Sets a state of a trajectory
   * @param idx The index of the state in the trajectory
   * @param state The state in which it should be set to
   */
  void Set(size_t idx, const State& state) {
    BOOST_ASSERT_MSG(idx >= Size(), "Cannot set state, out of bound");
    pos_traj_valid_ = false;
    traj_[idx] = state;
  }
  /**
   * \brief Adds a state to the end of a trajectory
   * @param state the state to add
   */
  void Append(const State& state) {
    pos_traj_valid_ = false;
    traj_.push_back(state);
  }
  /**
   * \brief Appends a trajectory to the end of this trajectory
   * @param traj the trajectory to add
   */
  void Append(const Trajectory<State>& traj) {
    pos_traj_valid_ = false;
    for(auto state : traj) {
        traj_.push_back(state);
    }
  }
  /**
   * \brief Adds a state to the end of a trajectory
   * @param state the state to add
   */
  // allows us to use the std::back_inserter
  void push_back(const State& state) {
    Append(state);
  }
  // NEEDS at least g++ 4.9
  /**
   * \brief Removes a state from the trajectory
   * @param c_it points to which state
   */
  void Erase(const_iterator c_it) {
    pos_traj_valid_ = false;
    // Necessary to be compatible with g++ 4.8 and older
    iterator it(ConvertConstIterToIter(c_it));
    traj_.erase(it);
  }
  /**
   * \brief Removes a state from the trajectory
   * @param it points to which state
   */
  void Erase(iterator it) {
    pos_traj_valid_ = false;
    traj_.erase(it);
  }
  /**
   * \brief Inserts a state into the trajectory
   * @param c_it the position in which the state will be inserted
   * @param state the state
   */
  void Insert(const_iterator c_it, const State &state) {
    pos_traj_valid_ = false;
    // Necessary to be compatible with g++ 4.8 and older
    iterator it(ConvertConstIterToIter(c_it));
    traj_.insert(it, state);
  }
 private:

  /**
   * \brief Generates a position specific trajectory from the current one
   */
  void GeneratePosTraj(void) {
    /* reserve memory */
    pos_traj_.clear();
    pos_traj_.reserve(traj_.size());
    /* copy all pos information*/
    std::for_each(traj_.begin(), traj_.end(), [this](const State& s)
    { pos_traj_.push_back(Vector3D(s.Pos()));});
    /* finally set flag */
    pos_traj_valid_ = true;
  }

  /**
   * \brief Returns a non-const iterator from a const type
   * @param c_it the const iterator
   * @return the non-const version
   */
  iterator ConvertConstIterToIter(const_iterator c_it) {
    iterator it(traj_.begin());
    std::advance(it, std::distance<const_iterator>(it, c_it));
    return it;
  }

  /*members*/
  Traj traj_;
  // special trajectory contains only pos information to allow for faster coll check. Done by lazy evaluation
  std::vector<Vector3D> pos_traj_;
  // parameter if pos_traj_ is valid, consistent with traj_
  bool pos_traj_valid_;
};

/**
 * \brief State for fixed wing dynamics
 */
class StateFW {
 public:

  /**
   * \Default Constructor
   */
  StateFW()
      : pos_(0.0, 0.0, 0.0),
        head_(0.0),
        speedxy_(0.0),
        speedz_(0.0),
        headrate_(0.0),
        time_(0.0) {
  }
  ;

  /**
   * \brief Initialization Constructor
   * @param pos position vector in meters
   * @param head heading scalar in radians
   * @param speedxy horizontal linear speed in m/s
   * @param speedz vertical speed in m/s
   * @param headrate angular velocity in rad/s
   * @param time time-stamp
   */
  StateFW(Vector3D pos, double head, double speedxy, double speedz,
          double headrate, double time)
      : pos_(pos),
        head_(head),
        speedxy_(speedxy),
        speedz_(speedz),
        headrate_(headrate),
        time_(time) {
  }
  ;

  /**
   * \brief Initialization Constructor
   * @param x position component (m)
   * @param y position component (m)
   * @param z position component (m)
   * @param head heading (rad)
   * @param speedxy horizontal speed (m/s)
   * @param speedz vertical speed (m/s)
   * @param headrate angular speed (m/s)
   * @param time time-stamp
   */
  StateFW(double x, double y, double z, double head, double speedxy,
          double speedz, double headrate, double time)
      : pos_(x, y, z),
        head_(head),
        speedxy_(speedxy),
        speedz_(speedz),
        headrate_(headrate),
        time_(time) {
  }
  ;
  typedef std::initializer_list<double> list_type;

  /**
   * \brief Initialization Constructor that takes in a list of float values
   * @param l a list
   */
  StateFW(list_type l) {
    BOOST_ASSERT_MSG(l.size() != 8, "initializer list has wrong size");

    pos_ = Vector3D(*l.begin(), *(l.begin() + 1), *(l.begin() + 2));
    head_ = *(l.begin() + 3);
    speedxy_ = *(l.begin() + 4);
    speedz_ = *(l.begin() + 5);
    headrate_ = *(l.begin() + 6);
    time_ = *(l.begin() + 7);
  }

  /**
   * \brief Returns whether this fixed wing state is equivalent to another (bit wise)
   * \todo  Do we need something like < 1e-9?
   *
   * @param state the state to compare to
   * @return true if they are deemed equal
   */
  bool Equal(const StateFW& state) const {
    // Do we need something like < 1e-9?
    if (state.Time() != this->Time() || state.Pos() != this->Pos()
        || state.SpeedXY() != this->SpeedXY()
        || state.SpeedZ() != this->SpeedZ()
        || state.Heading() != this->Heading()
        || state.HeadingRate() != this->HeadingRate()) {

      return false;
    }
    return true;
  }

  /**
   * \brief Returns whether this fixed wing state is equivalent to another (bit wise)
   * @param state the state to compare to
   * @return true if they are deemed equal
   */
  bool operator ==(const StateFW& state) const {
    return Equal(state);
  }

  /**
   * \brief Sets the state to another fixed wing state
   * @param state reference to the other fixed wing state
   */
  void operator =(const StateFW& state) {  // needed to use std::back_inserter on eml::Trajectory
    this->pos_ = state.Pos();
    this->head_ = state.Head();
    this->speedxy_ = state.SpeedXY();
    this->speedz_ = state.SpeedZ();
    this->headrate_ = state.HeadRate();
    this->time_ = state.Time();
  }

  /**
   * \brief Output stream for fixed wing state
   * @param os
   * @param state
   * @return
   */
  friend std::ostream& operator <<(std::ostream& os, const StateFW& state) {
    os << "[" << state.Pos()[0] << "," << state.Pos()[1] << ","
        << state.Pos()[2] << "," << state.Head() << "," << state.SpeedXY()
        << "," << state.SpeedZ() << "," << state.HeadRate() << ","
        << state.Time() << "]";
    return os;
  }

  /**
   * \brief Returns the position of the state
   * @return three dimensional vector (m)
   */
  /*Getter*/
  Vector3D Pos(void) const {
    return pos_;
  }
  /**
   * \brief Returns the linear velocity of the state
   * @return three dimensional vector (horizonatal speed, 0, vertical speed) (m/s)
   */
  Vector3D BodyVel(void) const {
    return Vector3D(speedxy_, 0.0, speedz_);
  }  // no velocity in y-direction
  /**
   * \brief Returns the horizontal, linear velocity
   * @return velocity (m/s)
   */
  double SpeedXY(void) const {
    return speedxy_;
  }
  /**
   * \brief Returns the vertical, linear velocity
   * @return velocity (m/s)
   */
  double SpeedZ(void) const {
    return speedz_;
  }
  /**
   * \brief Returns the horizontal heading
   * @return heading in radians
   */
  double Heading(void) const {
    return head_;
  }
  /**
   * \brief Returns the horizontal heading
   * @return radians
   */
  double Head(void) const {
    return head_;
  }
  /**
   * \brief Returns the angular velocity
   * @return rad/s
   */
  double HeadingRate(void) const {
    return headrate_;
  }
  /**
   * \brief Returns the angular velocity
   * @return rad/s
   */
  double HeadRate(void) const {
    return headrate_;
  }
  /**
   * \brief Returns the implied roll angle
   * \todo define emergency maneuver g
   *
   * @return radians
   */
  //TODO define emergency maneuver g
  double Roll(void) const {/*CA_ERROR("Not tested");*/
    return -atan((speedxy_ / 9.81) * headrate_);
  }
  /**
   * \brief Return the time-stamp
   * @return seconds
   */
  double Time(void) const {
    return time_;
  }
  /*Setter*/
  /**
   * \brief Sets the position
   * @param pos three-dimensional vector (m)
   */
  void Pos(const Vector3D &pos) {
    pos_ = pos;
  }
  /**
   * \brief Set the horizontal speed
   * @param speed (m/s)
   */
  void SpeedXY(double speed) {
    speedxy_ = speed;
  }
  /**
   * \brief Set the vertical speed
   * @param speed (m/s)
   */
  void SpeedZ(double speed) {
    speedz_ = speed;
  }
  /**
   * \brief Set the horizontal heading
   * @param heading (rad)
   */
  void Heading(double heading) {
    head_ = heading;
  }
  /**
   * \brief Set the horizontal heading
   * @param heading (rad)
   */
  void Head(double heading) {
    head_ = heading;
  }
  /**
   * \brief Set the horizontal angular rate
   * @param heading_rate (rad/s)
   */
  void HeadingRate(double heading_rate) {
    headrate_ = heading_rate;
  }
  /**
   * \brief Set the horizontal angular rate
   * @param heading_rate (rad/s)
   */
  void HeadRate(double heading_rate) {
    headrate_ = heading_rate;
  }
  /**
   * \brief Set the time-stamp
   * @param time seconds
   */
  void Time(double time) {
    time_ = time;
  }
 private:
  Vector3D pos_;
  double head_;
  double speedxy_;
  double speedz_;
  double headrate_;  // roll
  double time_;  // not really a state, but easier like this
};

//TODO make trajectory template based!!!!

}//namespace emergengy_maneuver
}  //namespace ca

#endif
