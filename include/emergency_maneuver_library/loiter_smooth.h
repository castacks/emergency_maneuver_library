/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/*
 * loiter_smooth.h
 *
 *  Created on: Apr 23, 2016
 *      Author: vdugar
 */

#ifndef EMERGENCY_MANEUVER_LIBRARY_INCLUDE_EMERGENCY_MANEUVER_LIBRARY_LOITER_SMOOTH_H_
#define EMERGENCY_MANEUVER_LIBRARY_INCLUDE_EMERGENCY_MANEUVER_LIBRARY_LOITER_SMOOTH_H_


#include "emergency_maneuver_library/trajectory.h"
#include <Eigen/Dense>

namespace ca {
namespace emergency_maneuver_library {

Eigen::MatrixXd GetSmoothClkLoiter();
Eigen::MatrixXd GetSmoothCClkLoiter();

class SmoothLoiter {
	constexpr static double g = 9.81;
public:

	SmoothLoiter(double x, double y, double z, double head_init, double bank_init,
			double airspeed, double time0, bool clockwise,
			Eigen::MatrixXd clk_em, Eigen::MatrixXd cclk_em, double em_init_bank)
		: x_(x), y_(y), z_(z),
		  head_init_(head_init),
		  bank_init_(bank_init),
		  airspeed_(airspeed),
		  time0_(time0),
		  clockwise_(clockwise),
		  clk_em_(clk_em),
		  cclk_em_(cclk_em),
		  em_init_bank_ (em_init_bank) {
	};

	SmoothLoiter(const StateFW &st, double bank_init, bool clock_wise,
			Eigen::MatrixXd clk_em,  Eigen::MatrixXd cclk_em, double em_init_bank)
		: x_(st.Pos()[0]),
		  y_(st.Pos()[1]),
		  z_(st.Pos()[2]),
		  head_init_(st.Heading()),
		  bank_init_(bank_init),
		  airspeed_(st.SpeedXY()),
		  time0_(st.Time()),
		  clockwise_(clock_wise),
		  clk_em_(clk_em),
		  cclk_em_(cclk_em),
		  em_init_bank_(em_init_bank)	{
	 };

	Trajectory<StateFW> GetClosedTrajectory(double time_res);

	std::vector<Eigen::Vector3d> GetClosedTrajectoryPoints(double time_res);

	// Returns the correct heading change from th1 to th2
	double HeadingDiff(double th1, double th2);


private:
	double x_, y_, z_;				// start point of loiter
	double head_init_;				// initial heading of loiter
	double bank_init_;				// initial bank
	double airspeed_;				// loiter airspeed
	double time0_;					// Initial time (s)
	bool clockwise_;				// 'Handed-ness' of the turn
	Eigen::MatrixXd clk_em_;			// clockwise turn file
	Eigen::MatrixXd cclk_em_;		// counter-clock turn file
	double em_init_bank_;			// Bank when the EM is triggered

	// Returns a rotation matrix about the Z-axis (counter-clock +ve, angle in rads)
	Eigen::Matrix3d GetRotationMatrix(double angle);

	// Returns the correct loiter depending on initial conditions
	Eigen::MatrixXd GetLoiter();

	// Returns the index along the loiter where the bank matches the initial bank
	unsigned int MatchBank(Eigen::MatrixXd& loit);

	// Returns the correct heading change from th1 to th2
//	double HeadingDiff(double th1, double th2);

};
} // emergency_maneuver_library
} // ca


#endif /* EMERGENCY_MANEUVER_LIBRARY_INCLUDE_EMERGENCY_MANEUVER_LIBRARY_LOITER_SMOOTH_H_ */
