/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/******************************************************************************
 * Filename: loiter.h
 * Description:
 *
 * Version: 1.0
 * Created: Mar 23 2015 08:59:40
 * Last modified: Nov 10 2015 03:48:24 PM
 * Revision: None
 *
 * Author:  althoff
 * e-mail:  althoff@andrew.cmu.edu
 *
 * Revision history
 * Date Author Remarks
 * Mar 23 2015 althoff File created.
 *
 ******************************************************************************/
#ifndef EMERGENCY_MANEUVER_LIBRARY_LOITER_H_
#define EMERGENCY_MANEUVER_LIBRARY_LOITER_H_

#include "emergency_maneuver_library/trajectory.h"

namespace ca {
namespace emergency_maneuver_library {

/**
 * \brief Contains information pertaining to a loiter maneuver
 */
class Loiter {
  constexpr static double g = 9.81;
 public:

  /* x,y,z NOT center, first point on loiter from transition trajectory?*/
  /**
   * \brief Initialization constructor using extended parameters.
   *
   * x,y,z NOT center, first point on loiter from transition trajectory?
   * @param x coordinate in meters
   * @param y coordinate in meters
   * @param z coordinate in meters
   * @param head heading in radians
   * @param radius in meters
   * @param speed forward velocity in meters per second
   * @param time0 initial time in seconds
   * @param clock_wise flag to indicate direction of loiter
   */
  Loiter(double x, double y, double z, double head, double radius, double speed,
         double time0, bool clock_wise)
      : x_(x),
        y_(y),
        z_(z),
        head_(head),
        radius_(radius),
        speed_(speed),
        time0_(time0),
        clock_wise_(clock_wise) {
  }
  ;

  // TODO test
  /**
   * \brief  Initialization constructor using state.
   * \todo test
   *
   * @param st
   * @param clock_wise
   */
  Loiter(const StateFW &st, bool clock_wise)
      : x_(st.Pos()[0]),
        y_(st.Pos()[1]),
        z_(st.Pos()[2]),
        head_(st.Heading()),
        radius_(st.SpeedXY() / std::abs(st.HeadingRate())),
        speed_(st.SpeedXY()),
        time0_(st.Time()),
        clock_wise_(clock_wise) {
        
        //TODO maybe nicer? If headingrate==0 we have a problem
        if (std::abs(st.HeadingRate()) <= 1e-9) {
            radius_ = 0.0;
        }
  }
  ;

  /*getter*/
  /**
   * \brief return loiter trajectory
   * \todo wrap to -pi / pi
   * \todo but to state description and push to Trajectory
   *
   * @param time_res specifies spacing between points in time
   * @return the trajectory
   */
  Trajectory<StateFW> GetClosedTrajectory(double time_res) const {
    // sample along the circle to get trajectory
    // TODO wrap to -pi / pi
    // TODO but to state description and push to Trajectory
    Trajectory<StateFW> traj;
    if (IsHover() == true) {
      // if is hover, reference traj contains 2 times the same state with different time, needed for inf maneuver
      double time(time0_);
      // headrate and speedz 0 by definition of hover
      StateFW state(Eigen::Vector3d(x_, y_, z_), head_, speed_, 0.0, 0.0, time);
      traj.Append(state);
      state.Time(time + time_res);
      traj.Append(state);
      return traj;
    }
    double phi_res(speed_ * time_res / radius_);
    phi_res = clock_wise_ == true ? -phi_res : phi_res;
    const Eigen::Vector3d center(GetCenter());
    //std::cout << "Loiter center: " << center << std::endl;
    const double phi0(atan2(y_ - center[1], x_ - center[0]));
    const double head_rate(GetHeadRate());
    const size_t steps(std::floor(2.0 * M_PI / std::abs(phi_res)) + 1);
    //std::cout << "num_steps: " << steps << std::endl;
    //std::cout << "phi_res" << phi_res << std::endl;
    for (size_t i(0); i < steps; ++i) {
      double phi(phi0 + i * phi_res);
      double head(head_ + i * phi_res);
      double time(time0_ + i * time_res);
      double tmp_x = cos(phi) * radius_ + center[0];
      double tmp_y = sin(phi) * radius_ + center[1];
      double tmp_z = z_;
      // head rate constant,speed_xy constant,speed_z 0
      StateFW state(Eigen::Vector3d(tmp_x, tmp_y, tmp_z), head, speed_, 0.0,
                    head_rate, time);
      traj.Append(state);
    }

    // special treatment for final point to close the trajectory
    // last phi step
    const double phi_step(2.0 * M_PI - std::abs((steps - 1) * phi_res));
    //std::cout << "phi_step: " << phi_step << std::endl;
    const double time_step = phi_step * radius_ / speed_;
    //std::cout << "time_step: " << time_step << std::endl;
    //std::cout << "final time: " << traj.Back().Time() << std::endl;
    //std::cout << "LAST STATE: " << traj.Back() << std::endl;
    //std::cout << "final time: " << traj.Back().Time()+time_step << std::endl;
    StateFW state(Eigen::Vector3d(x_, y_, z_), head_, speed_, 0.0, head_rate,
                  traj.Back().Time() + time_step);
    traj.Append(state);
    //debug
    //for (auto i : traj)
    //	std::cout << i << std::endl;

    return traj;
  }

  /**
   * \brief returns x coordinate of first state on loiter
   * @return coordinate in meters
   */
  double GetXPos(void) const {
    return x_;
  }

  /**
   * \brief returns y coordinate of first state on loiter
   * @return coordinate in meters
   */
  double GetYPos(void) const {
    return y_;
  }
  /**
   * \brief returns z coordinate of first state on loiter
   * @return coordinate in meters
   */
  double GetZPos(void) const {
    return z_;
  }
  /**
   * \brief returns heading of first state on loiter
   * @return heading in radians
   */
  double GetHead(void) const {
    return head_;
  }
  /**
   * \brief returns heading rate of loiter manuever
   * @return heading rate in radians per second
   */
  double GetHeadRate(void) const {
    return IsClockwise() == true ? -speed_ / radius_ : speed_ / radius_;
  }
  /**
   * \brief returns radius of loiter
   * @return radius in meters
   */
  double GetRadius(void) const {
    return radius_;
  }
  /**
   * \brief returns forward speed of loiter
   * @return speed in meters per second
   */
  double GetSpeed(void) const {
    return speed_;
  }
  /**
   * \brief returns roll angle during loiter
   * @return roll angle in radians
   */
  double GetRoll(void) const {
    const double roll(atan((speed_ * speed_) / (g * radius_)));
    return clock_wise_ == true ? -roll : roll;
  }
  /**
   * \brief returns the circumference of the loiter
   * @return circumference in meters
   */
  double GetCircumfence(void) const {
    return 2.0 * M_PI * radius_;
  }
  /**
   * \brief returns the time to complete the whole loiter maneuver
   * @return time in seconds
   */
  double GetTime(void) const {
    return GetCircumfence() / speed_;
  }
  /**
   * \brief returns whether the loiter maneuver should be considered a regular hover
   * @return true if close to hover
   */
  bool IsHover(void) const {
    return radius_ < 1e-9 ? true : false;
  }
  /**
   * \brief returns whether the loiter is moving in a clockwise or counter-clockwise direction
   * @return true if clockwise, false if counter-clockwise
   */
  bool IsClockwise(void) const {
    return clock_wise_;
  }
  /**
   * \brief Returns the center point of the loiter
   * \todo  wrap to -pi pi
   *
   * @return Three dimensional position in meters
   */
  Eigen::Vector3d GetCenter(void) const {
    // get tangential dir
    //TODO wrap to -pi pi
    double center_head;
    if (IsClockwise() == true)
      center_head = head_ - M_PI / 2.0;
    else
      center_head = head_ + M_PI / 2.0;

    Eigen::Vector2d tmp(x_, y_);
    tmp += Eigen::Vector2d(cos(center_head), sin(center_head)) * radius_;
    return Eigen::Vector3d(tmp[0], tmp[1], z_);
  }
 private:
  double x_;
  double y_;
  double z_;
  double head_;
  double radius_;
  double speed_;
  double time0_;
  bool clock_wise_;
};

}				//namespace eml
}				//namespace ca

#endif
// vim:sw=3:ts=3
