/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/******************************************************************************
 * Filename: open_loop_emergency_maneuver.h
 * Description:
 *
 * Version: 1.0
 * Created: Mar 20 2015 13:37:44
 * Last modified: Apr 01 2015 01:44:11 PM
 * Revision: None
 *
 * Author:  althoff
 * e-mail:  althoff@andrew.cmu.edu
 *
 * Revision history
 * Date Author Remarks
 * Mar 20 2015 althoff File created.
 *
 ******************************************************************************/
#ifndef _OPEN_LOOP_EMERGENCY_MANEUVER_H_
#define _OPEN_LOOP_EMERGENCY_MANEUVER_H_

#include <limits>
#include "emergency_maneuver_library/loiter.h"
#include "emergency_maneuver_library/loiter_smooth.h"

namespace ca {

/**
 * \brief Emergency maneuver library
 */
namespace emergency_maneuver_library {

/**
 * \brief Generates a set of emergency maneuvers form a hypothetical origin state
 *
 * \todo should make functions private
 */
class OpenLoopEmergencyManeuver {
 public:

  /**
   * \brief Initialization constructor
   * @param high_speed_thresh how fast initial state must be to execute high-speed maneuvers
   * @param acc_xy assumed constant horizontal acceleration in m/s^2
   * @param vel_z_max vertical velocity limit in meters
   * @param acc_z_max vertical acceleration limit in m/s^2
   * @param acc_z_res vertical acceleration resolution in m/s^2. Dictates variation between maneuvers.
   * @param roll_max banking angle limit in radians
   * @param roll_max_res banking angle resolution in radians.  Dictates variation between maneuvers.
   * @param roll_rate bank assumed constant bank angle rate in rad/s.
   * @param head_rate_acc heading acceleration rate limit in rad/s^2.
   * @param head_rate_max heading rate limit in rad/s.
   * @param head_rate_max_res heading rate resolution in rad/s.  Dictates variation between maneuvers.
   * @param time_res Resolution in terms of seconds between states along a single maneuver.
   * @param high_speed_maneuver_max_velocity (optional) largest velocity acceptable in a high speed emergency maneuver.  Will be set to if not specified.
   */
  OpenLoopEmergencyManeuver(double high_speed_thresh, double acc_xy,
                            double vel_z_max, double acc_z_max,
                            double acc_z_res, double roll_max,
                            double roll_max_res, double roll_rate,
                            double head_rate_acc, double head_rate_max,
                            double head_rate_max_res, double time_res,
							Eigen::MatrixXd &clk_em, Eigen::MatrixXd &cclk_em, double high_speed_maneuver_max_velocity = -1)
      : high_speed_thresh_(high_speed_thresh),
        acc_xy_(acc_xy),
        vel_z_max_(vel_z_max),
        acc_z_max_(acc_z_max),
        acc_z_res_(acc_z_res),
        roll_max_(roll_max),
        roll_max_res_(roll_max_res),
        roll_rate_(roll_rate),
        head_rate_acc_(head_rate_acc),
        head_rate_max_(head_rate_max),
        head_rate_max_res_(head_rate_max_res),
        time_res_(time_res),
		clk_em_(clk_em),
		cclk_em_(cclk_em), high_speed_maneuver_max_velocity_(std::max(high_speed_thresh,high_speed_maneuver_max_velocity)) {
  }


  /* public funtion to distinguish between correct private function*/

  /**
   * \brief Retrieve a set of emergency maneuvers from a starting state
   * @param state the initial state the maneuvers will start from
   * @return The series of possible emergency maneuvers
   */
  std::vector<std::pair<Trajectory<StateFW>, Loiter> > GetEmergencyTrajectories(
      const StateFW &state) const;

  /**
   * \brief Retrieve a set of emergency maneuvers from a starting state
   * @param state the initial state the maneuvers will start from
   * @return The series of possible emergency maneuvers
   */
  std::vector<std::pair<Trajectory<StateFW>, SmoothLoiter> > GetSmoothEmergencyTrajectories(
      const StateFW &state) const;

  /*should make functions private?*/

  /**
   * \brief Retrieve a set of high-speed maneuvers from an origin state
   * @param state the initial state
   * @return The series of possible high-speed emergency maneuvers
   */
  std::vector<std::pair<Trajectory<StateFW>, Loiter> > GetHighSpeedEmergencyTrajectories(
      const StateFW &state) const;

  /**
   * \brief Retrieve a set of low-speed maneuvers from an origin state
   * @param state the initial state
   * @return There series of possible low-speed emergency maneuvers
   */
  std::vector<std::pair<Trajectory<StateFW>, Loiter> > GetSlowSpeedEmergencyTrajectories(
      const StateFW &state) const;

  /**
   * \brief Retrieve a set of high-speed maneuvers from an origin state, using pre-defined smooth loiters
   * @param state the initial state
   * @return The series of possible high-speed emergency maneuvers
   */
  std::vector<std::pair<Trajectory<StateFW>, SmoothLoiter> > GetSmoothHighSpeedEmergencyTrajectories(
      const StateFW &state) const;

  /**
   * \brief Retrieve a set of slow-speed maneuvers from an origin state, using pre-defined smooth loiters
   * @param state the initial state
   * @return There series of possible low-speed emergency maneuvers
   */
  std::vector<std::pair<Trajectory<StateFW>, SmoothLoiter> > GetSmoothSlowSpeedEmergencyTrajectories(
      const StateFW &state) const;

  /**
   * \brief Generate a single high-speed emergency maneuver
   * @param state_init initial state
   * @param vxy_min lower bound horizontal linear speed limit (m/s)
   * @param vz_max vertical speed limit (m/s)
   * @param roll_rate constant bank angle rate (rad/s)
   * @param roll_max bank angle limit (rad)
   * @param acc_xy constant horizontal linear acceleration (m/s^2)
   * @param acc_z constant vertical linear acceleration (m/s^2)
   * @return A list of of forward motion states followed by a loiter
   */
  std::pair<Trajectory<StateFW>, Loiter> GetHighSpeedEmergencyTrajectory(
      const StateFW &state_init, double vxy_min, double vz_max,
      double roll_rate, double roll_max, double acc_xy, double acc_z) const;
  /**
   * \brief Generate a single low-speed emergency maneuver
   * @param state_init initial state
   * @param vxy_min lower bound horizontal linear speed limit (m/s)
   * @param vz_max vertical speed limit (m/s)
   * @param head_rate_acc constant heading acceleration rate (rad/s^2)
   * @param head_rate_max heading rate limit (rad/s)
   * @param acc_xy constant horizontal linear acceleration (m/s^2)
   * @param acc_z constant vertical linear acceleration (m/s^2)
   * @return A list of forward motion states followed by a loiter
   */
  std::pair<Trajectory<StateFW>, Loiter> GetSlowSpeedEmergencyTrajectory(
      const StateFW &state_init, double vxy_min, double vz_max,
      double head_rate_acc, double head_rate_max, double acc_xy,
      double acc_z) const;

  /**
   * \brief Generate a single high-speed, smooth emergency maneuver (using pre-computed curvature primitives)
   * @param state_init initial state
   * @param vxy_min lower bound horizontal linear speed limit (m/s)
   * @param vz_max vertical speed limit (m/s)
   * @param roll_rate constant bank angle rate (rad/s)
   * @param roll_max bank angle limit (rad)
   * @param acc_xy constant horizontal linear acceleration (m/s^2)
   * @param acc_z constant vertical linear acceleration (m/s^2)
   * @param minimize_height_deviation flag to direct generator to minimize z deviation as much as possible
   * @return A list of of forward motion states followed by a loiter
   */
  std::pair<Trajectory<StateFW>, SmoothLoiter> GetSmoothHighSpeedEmergencyTrajectory(
      const StateFW &state_init, double vxy_min, double vz_max,
      double roll_rate, double roll_max, double acc_xy, double acc_z, bool minimize_height_deviation = false) const;

  /**
   * \brief Generate a single low-speed smooth emergency maneuver (using pre-computed curvature primitives)
   * @param state_init initial state
   * @param vxy_min lower bound horizontal linear speed limit (m/s)
   * @param vz_max vertical speed limit (m/s)
   * @param head_rate_acc constant heading acceleration rate (rad/s^2)
   * @param head_rate_max heading rate limit (rad/s)
   * @param acc_xy constant horizontal linear acceleration (m/s^2)
   * @param acc_z constant vertical linear acceleration (m/s^2)
   * @return A list of forward motion states followed by a loiter
   */
  std::pair<Trajectory<StateFW>, SmoothLoiter> GetSmoothSlowSpeedEmergencyTrajectory(
      const StateFW &state_init, double vxy_min, double vz_max,
      double head_rate_acc, double head_rate_max, double acc_xy,
      double acc_z) const;



 private:

  /**
   * \brief Return vertical ascent/descent motion
   * @param num_pts number of states the motion should contain
   * @param z0 initial height (m)
   * @param vz0 initial vertical velocity (m/s)
   * @param vz_max vertical velocity limit (m/s)
   * @param acc_z constant vertical acceleration rate (m/s^2)
   * @param z_pos (output) heights of states in motion (m)
   * @param z_vel (output) vertical speeds of states in motion (m/s)
   * @param minimize_height_deviation flag to direct generator to minimize z deviation as much as possible
   */
  void GetZEmergencyTrajectory(size_t num_pts, double z0, double vz0,
                               double vz_max, double acc_z,
                               std::vector<double> &z_pos,
                               std::vector<double> &z_vel, bool minimize_height_deviation = false) const;

  /**
   * \brief Not implemented
   * @param state
   * @return
   */
  std::vector<std::pair<Trajectory<StateFW>, Loiter> > GetEmergencyWindTrajectories(
      const StateFW &state) const;

  /**
   * \brief Not implemented
   * @param state
   * @param max_roll
   * @param acc_z
   * @return
   */
  std::pair<Trajectory<StateFW>, Loiter> GetEmergencyWindTrajectory(
      const StateFW &state, double max_roll, double acc_z) const;

  /**
   * \brief Calculates turning radius and curvature of turn
   * @param (output) radius turning radius
   * @param (output) w curvature
   * @param velocity forward velocity assumed during turn
   * @param roll bank angle assumed during turn
   */
  void GetRadiusAndOmega(double &radius, double &w, double velocity,
                         double roll) const;

  /*members*/
  double high_speed_thresh_;
  double acc_xy_;  //paramMaximumDecceleration = 0.1*g;
  double high_speed_maneuver_max_velocity_;

  double vel_z_max_;  //paramVzMax = 2.0;
  double acc_z_max_;  // = 0.1*g;
  double acc_z_res_;  //paramAccZResolution = 0.2 * paramAccZMax;

  double roll_max_;  //paramMaximumRoll = 30.0*M_PI/180.0;
  double roll_max_res_;  //paramRollResolution = paramMaximumRoll/3.0;
  double roll_rate_;  //paramRollRate = 15.0*M_PI/180.0;

  double head_rate_acc_;  // for slow speeds, roll ignored
  double head_rate_max_;
  double head_rate_max_res_;

  double time_res_;  //paramTimeResolution = 0.1;

  // EML maneuvers
  Eigen::MatrixXd clk_em_;
  Eigen::MatrixXd cclk_em_;
};
}  //namespace eml
}  //namespace ca

#endif
// vim:sw=3:ts=3
