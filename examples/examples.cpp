/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/*
 * examples.cpp
 *
 *  Created on: Apr 23, 2016
 *      Author: vdugar
 */

#include <iostream>
#include <emergency_maneuver_library/open_loop_emergency_maneuver.h>
#include <emergency_maneuver_library/loiter_smooth.h>
#include <emergency_maneuver_library/trajectory.h>
#include <math.h>
#include <fstream>

using namespace ca;
namespace eml = emergency_maneuver_library;

//void testHeadingDiff() {
//	eml::SmoothLoiter loit(0, 0, 0, 0, 0, 0, 0, true);
//	std::cout << "pi/2 - 0 = " << loit.HeadingDiff(0, M_PI/2) << "\n";
//	std::cout << "0 - pi = " << loit.HeadingDiff(M_PI, 0) << "\n";
//	std::cout << "pi/6 - 2pi = " << loit.HeadingDiff(2*M_PI, M_PI/6) << "\n";
//}

//void testInput() {
////	std::ifstream fin("/home/vdugar/test.csv");
////
////	Eigen::MatrixXd data(2, 3);
////
////	double d;
//////	while(std::getline(fin, linestr, ',')) {
//////		std::istringstream(linestr) >> d;
//////		std::cout << d << "\n";
//////	}
////	for(int i = 0; i < 2; ++i) {
////		for(int j = 0; j < 3; ++j) {
////			fin >> d;
////			data(i, j) = d;
////		}
////	}
////	std::cout << data;
//
//	Eigen::MatrixXd data = ca::emergency_maneuver_library::GetSmoothCClkLoiter();
//	std::cout << data.rows() << " " << data.cols();
//}

//void testLoiter() {
//	// define loiter
////	eml::SmoothLoiter loit(100, 200, -341, M_PI/3, 0.99*M_PI/6, 30, 0, false);
//	eml::SmoothLoiter loit(0, 0, 0, 0, 0, 30, 0, false);
//
//	// get trajectory
//	eml::Trajectory<eml::StateFW> traj = loit.GetClosedTrajectory(0.01);
//	std::cout << "got loiter\n";
//
//	// Write to file
//	std::FILE *f;
//	f = fopen("traj.txt", "w");
//
//	eml::Trajectory<eml::StateFW>::const_iterator iter;
//	for(iter = traj.Begin(); iter != traj.End(); iter++) {
//		eml::StateFW state = *iter;
//		Eigen::Vector3d pos = state.Pos();
//		Eigen::Vector3d vel = state.BodyVel();
//		double heading = state.Heading();
//		double head_rate = state.HeadingRate();
//		double roll = state.Roll();
//		double time = state.Time();
//
//		fprintf(f, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n",
//				pos(0), pos(1), pos(2), heading, roll,
//				vel(0), vel(2), head_rate, time);
//	}
//
//}

//void testEml() {
//
//	double time_step = 3.0;
//	// Define open loop eml object
//	eml::OpenLoopEmergencyManeuver ol_em(20, -0.981, 2.54, 0.7, 0.35,
//			20*M_PI/180, 10*M_PI/180, 10*M_PI/180, 0.05, 0.1, 0.05, time_step,
//			"/home/vdugar/work/smooth_eml_matlab/clockwise.txt",
//			"/home/vdugar/work/smooth_eml_matlab/counterclock.txt");
//
//	// Define initial conditions
//	eml::StateFW state(400, 30, -200, M_PI/3, 40, 0, 9.81*tan(0*M_PI/180)/40, 20);
//
//	std::cout << state << "\n";
//
//	// Get emergency trajectories
//	std::vector<std::pair<eml::Trajectory<eml::StateFW>, eml::SmoothLoiter> > smooth_set =
//			ol_em.GetSmoothEmergencyTrajectories(state);
//
//	std::vector<std::pair<eml::Trajectory<eml::StateFW>, eml::Loiter> > old_set =
//			ol_em.GetEmergencyTrajectories(state);
//
//	std::cout << smooth_set.size() << ", " << old_set.size() << "\n";
//
//	// Write both to file
//	int ind = 11;
//	std::pair<eml::Trajectory<eml::StateFW>, eml::SmoothLoiter> smooth_traj = smooth_set[ind];
//	std::pair<eml::Trajectory<eml::StateFW>, eml::Loiter> old_traj = old_set[ind];
//	std::FILE *f_new_tran, *f_new_loit, *f_old;
//	f_new_tran = fopen("new_tran.txt", "w");
//	f_new_loit = fopen("new_loit.txt", "w");
//	f_old = fopen("old.txt", "w");
//
//	// write new
//	eml::Trajectory<eml::StateFW> trans = smooth_traj.first;
//	eml::Trajectory<eml::StateFW> loit = smooth_traj.second.GetClosedTrajectory(time_step);
//	eml::Trajectory<eml::StateFW>::const_iterator iter;
//	for(iter = trans.Begin(); iter != trans.End(); iter++) {
//		eml::StateFW state = *iter;
//		Eigen::Vector3d pos = state.Pos();
//		Eigen::Vector3d vel = state.BodyVel();
//		double heading = state.Heading();
//		double head_rate = state.HeadingRate();
//		double roll = state.Roll();
//		double time = state.Time();
//
//		fprintf(f_new_tran, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n",
//				pos(0), pos(1), pos(2), heading, roll,
//				vel(0), vel(2), head_rate, time);
//	}
//	for(iter = loit.Begin(); iter != loit.End(); iter++) {
//		eml::StateFW state = *iter;
//		Eigen::Vector3d pos = state.Pos();
//		Eigen::Vector3d vel = state.BodyVel();
//		double heading = state.Heading();
//		double head_rate = state.HeadingRate();
//		double roll = state.Roll();
//		double time = state.Time();
//
//		fprintf(f_new_loit, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n",
//				pos(0), pos(1), pos(2), heading, roll,
//				vel(0), vel(2), head_rate, time);
//	}
//
//	// write old
//	trans = old_traj.first;
//	loit = old_traj.second.GetClosedTrajectory(0.01);
//	for(iter = trans.Begin(); iter != trans.End(); iter++) {
//		eml::StateFW state = *iter;
//		Eigen::Vector3d pos = state.Pos();
//		Eigen::Vector3d vel = state.BodyVel();
//		double heading = state.Heading();
//		double head_rate = state.HeadingRate();
//		double roll = state.Roll();
//		double time = state.Time();
//
//		fprintf(f_old, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n",
//				pos(0), pos(1), pos(2), heading, roll,
//				vel(0), vel(2), head_rate, time);
//	}
//	for(iter = loit.Begin(); iter != loit.End(); iter++) {
//		eml::StateFW state = *iter;
//		Eigen::Vector3d pos = state.Pos();
//		Eigen::Vector3d vel = state.BodyVel();
//		double heading = state.Heading();
//		double head_rate = state.HeadingRate();
//		double roll = state.Roll();
//		double time = state.Time();
//
//		fprintf(f_old, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n",
//				pos(0), pos(1), pos(2), heading, roll,
//				vel(0), vel(2), head_rate, time);
//	}
//}

Eigen::MatrixXd GetSmoothLoiterFromFile(std::string fn) {
	int r(699), c(13);
	Eigen::MatrixXd data(r, c);

	std::ifstream fin(fn);

	double d;
	for(int i(0); i < r; ++i) {
		for(int j(0); j < c; ++j) {
			fin >> d;
			data(i, j) = d;
		}
	}

	return data;
}

void storeTraj() {
	double time_step = 0.1;
    Eigen::MatrixXd clk_em = GetSmoothLoiterFromFile("/home/vdugar/work/workspaces/acs_baseline_3a/src/emergency_maneuver_library/parameters/clockwise.txt");
    Eigen::MatrixXd cclk_em = GetSmoothLoiterFromFile("/home/vdugar/work/workspaces/acs_baseline_3a/src/emergency_maneuver_library/parameters/counterclock.txt");
	eml::OpenLoopEmergencyManeuver ol_em(20, -0.7, 2.54, 0.7, 0.35,
			0.2618, 0.1309, 0.0873, 0.05, 0.1, 0.05, time_step,
			clk_em,
			cclk_em);

	// Define initial conditions
	eml::StateFW state(400, 30, -200, M_PI/3, 40, 0, 9.81*tan(0*M_PI/180)/40, 20);

	// Get emergency maneuvers
	std::vector<std::pair<eml::Trajectory<eml::StateFW>, eml::SmoothLoiter> >
	maneuvers (ol_em.GetSmoothEmergencyTrajectories(state));

	// Parse and save to file
	eml::Trajectory<eml::StateFW>::const_iterator iter;
	eml::Trajectory<eml::StateFW> trans;
	eml::Trajectory<eml::StateFW> loit;
	char fn[50];
	FILE *out_file;
	for(int i(0); i < maneuvers.size(); ++i) {
		std::pair<eml::Trajectory<eml::StateFW>, eml::SmoothLoiter> smooth_traj = maneuvers[i];
		trans = smooth_traj.first;
		loit = smooth_traj.second.GetClosedTrajectory(time_step);

		sprintf(fn, "%d.txt", i+1);
		out_file = fopen(fn, "w");
		for(iter = trans.Begin(); iter != trans.End(); iter++) {
			eml::StateFW state = *iter;
			Eigen::Vector3d pos = state.Pos();
			Eigen::Vector3d vel = state.BodyVel();
			double heading = state.Heading();
			double head_rate = state.HeadingRate();
			double roll = state.Roll();
			double time = state.Time();

			fprintf(out_file, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n",
					pos(0), pos(1), pos(2), heading, roll,
					vel(0), vel(2), head_rate, time);
		}

		for(iter = loit.Begin(); iter != loit.End(); iter++) {
			eml::StateFW state = *iter;
			Eigen::Vector3d pos = state.Pos();
			Eigen::Vector3d vel = state.BodyVel();
			double heading = state.Heading();
			double head_rate = state.HeadingRate();
			double roll = state.Roll();
			double time = state.Time();

			fprintf(out_file, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n",
					pos(0), pos(1), pos(2), heading, roll,
					vel(0), vel(2), head_rate, time);
		}

		fclose(out_file);
	}
}

int main() {
//	testHeadingDiff();
//	testInput();
//	testLoiter();
	storeTraj();
}


