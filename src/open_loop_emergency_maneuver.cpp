/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
#include "emergency_maneuver_library/open_loop_emergency_maneuver.h"
#include "exception/exception.h"
#include "math_utils/math_utils.h"
#include <ros/console.h>

using namespace ca;
using namespace emergency_maneuver_library;


std::vector<std::pair<Trajectory<StateFW>,Loiter> > OpenLoopEmergencyManeuver::
GetEmergencyTrajectories(const StateFW &state) const {
    //std::cout << "State generating EmergencyManeuver: " << state << std::endl;
    if (state.SpeedXY() > high_speed_thresh_) {
        return GetHighSpeedEmergencyTrajectories(state);
    }
    else {
        return GetSlowSpeedEmergencyTrajectories(state);
    }
}

std::vector<std::pair<Trajectory<StateFW>, SmoothLoiter> > OpenLoopEmergencyManeuver::
GetSmoothEmergencyTrajectories(const StateFW &state) const {

   if (state.SpeedXY() > high_speed_thresh_) {
        return GetSmoothHighSpeedEmergencyTrajectories(state);
    }
    else {
    	return GetSmoothSlowSpeedEmergencyTrajectories(state);
    }
}


std::vector<std::pair<Trajectory<StateFW>,Loiter> > OpenLoopEmergencyManeuver::
GetSlowSpeedEmergencyTrajectories(const StateFW &state) const {
    BOOST_ASSERT_MSG(state.SpeedXY() <= high_speed_thresh_, "Speed too high to use SlowSpeed Emergency Trajectory");
    const double vel_xy_min(0.0);// we want to reach hover
    std::vector<std::pair<Trajectory<StateFW>,Loiter> > traj_em_set;
    size_t num_zval (std::floor(acc_z_max_ / acc_z_res_));
    for(size_t i(0); i<(2*num_zval+1); ++i) {    
        double acc_z ( (-static_cast<double>(num_zval)+i) * acc_z_res_);
        if (state.SpeedZ()!= 0 && acc_z == 0) continue; // cant reach level loiter
        for (double head_rate(0.0); head_rate <= head_rate_max_; head_rate+=head_rate_max_res_) {
            //std::cout << "head_rate: " << head_rate << std::endl;
            traj_em_set.push_back((GetSlowSpeedEmergencyTrajectory(state, vel_xy_min, vel_z_max_, head_rate_acc_, head_rate, acc_xy_, acc_z)));
            if (head_rate!=0)//right turn            
                traj_em_set.push_back((GetSlowSpeedEmergencyTrajectory(state, vel_xy_min, vel_z_max_, -head_rate_acc_, head_rate, acc_xy_, acc_z)));
        }
    }
    return traj_em_set;
}
std::vector<std::pair<Trajectory<StateFW>,Loiter> > OpenLoopEmergencyManeuver::
GetHighSpeedEmergencyTrajectories(const StateFW &state) const {
  double min_xy_vel = std::min(state.SpeedXY(), high_speed_maneuver_max_velocity_);
    std::vector<std::pair<Trajectory<StateFW>,Loiter> > traj_em_set;
    size_t num_zval (std::floor(acc_z_max_ / acc_z_res_));
    for(size_t i(0); i<(2*num_zval+1); ++i) {    
        double acc_z ( (-static_cast<double>(num_zval)+i) * acc_z_res_);
        if (state.SpeedZ()!= 0 && acc_z == 0) continue; // cant reach level loiter
        //for (double max_roll(0.0); max_roll <= roll_max_; max_roll+=roll_max_res_) {
        for (double max_roll(roll_max_res_); max_roll <= roll_max_; max_roll+=roll_max_res_) {
            //std::cout << "max_roll: " << max_roll << std::endl;
            traj_em_set.push_back((GetHighSpeedEmergencyTrajectory(state, min_xy_vel, vel_z_max_, roll_rate_, max_roll, acc_xy_, acc_z)));
            if (max_roll!=0.0) // left turn
                traj_em_set.push_back((GetHighSpeedEmergencyTrajectory(state, min_xy_vel, vel_z_max_, -roll_rate_, max_roll, acc_xy_, acc_z)));
        }
    }
    return traj_em_set;
}

std::vector<std::pair<Trajectory<StateFW>, SmoothLoiter> > OpenLoopEmergencyManeuver::
GetSmoothHighSpeedEmergencyTrajectories(const StateFW &state) const {
    double min_xy_vel = high_speed_maneuver_max_velocity_;
    std::vector<std::pair<Trajectory<StateFW>, SmoothLoiter> > traj_em_set;
    size_t num_zval (std::floor(acc_z_max_ / acc_z_res_));
    for(size_t i(0); i<(2*num_zval+1); ++i) {
        double acc_z ( (-static_cast<double>(num_zval)+i) * acc_z_res_);
        if (state.SpeedZ()!= 0 && acc_z == 0) continue; // cant reach level loiter
        //for (double max_roll(0.0); max_roll <= roll_max_; max_roll+=roll_max_res_) {
        for (double max_roll(roll_max_res_); max_roll <= roll_max_; max_roll+=roll_max_res_) {
            //std::cout << "max_roll: " << max_roll << std::endl;
            traj_em_set.push_back((GetSmoothHighSpeedEmergencyTrajectory(state, min_xy_vel, vel_z_max_, roll_rate_, max_roll, acc_xy_, acc_z)));
            if (max_roll!=0.0) // left turn
                traj_em_set.push_back((GetSmoothHighSpeedEmergencyTrajectory(state, min_xy_vel, vel_z_max_, -roll_rate_, max_roll, acc_xy_, acc_z)));

            //Motions for minimizing height deviation
            if(acc_z >= (acc_z_max_- acc_z_res_))
            {
              traj_em_set.push_back((GetSmoothHighSpeedEmergencyTrajectory(state, min_xy_vel, vel_z_max_, roll_rate_, max_roll, acc_xy_, acc_z, true)));
              if (max_roll!=0.0) // left turn
                  traj_em_set.push_back((GetSmoothHighSpeedEmergencyTrajectory(state, min_xy_vel, vel_z_max_, -roll_rate_, max_roll, acc_xy_, acc_z, true)));
            }
        }
    }
    return traj_em_set;
}

std::vector<std::pair<Trajectory<StateFW>, SmoothLoiter> > OpenLoopEmergencyManeuver::
GetSmoothSlowSpeedEmergencyTrajectories(const StateFW &state) const {
    BOOST_ASSERT_MSG(state.SpeedXY() <= high_speed_thresh_, "Speed too high to use SlowSpeed Emergency Trajectory");
    const double vel_xy_min(0.0);// we want to reach hover
    std::vector<std::pair<Trajectory<StateFW>, SmoothLoiter> > traj_em_set;
    size_t num_zval (std::floor(acc_z_max_ / acc_z_res_));
    for(size_t i(0); i<(2*num_zval+1); ++i) {
        double acc_z ( (-static_cast<double>(num_zval)+i) * acc_z_res_);
        if (state.SpeedZ()!= 0 && acc_z == 0) continue; // cant reach level loiter
        for (double head_rate(0.0); head_rate <= head_rate_max_; head_rate+=head_rate_max_res_) {
            //std::cout << "head_rate: " << head_rate << std::endl;
            traj_em_set.push_back((GetSmoothSlowSpeedEmergencyTrajectory(state, vel_xy_min, vel_z_max_, head_rate_acc_, head_rate, acc_xy_, acc_z)));
            if (head_rate!=0)//right turn
                traj_em_set.push_back((GetSmoothSlowSpeedEmergencyTrajectory(state, vel_xy_min, vel_z_max_, -head_rate_acc_, head_rate, acc_xy_, acc_z)));
        }
    }
    return traj_em_set;
}

std::pair<Trajectory<StateFW>,Loiter> OpenLoopEmergencyManeuver::
GetHighSpeedEmergencyTrajectory(const StateFW &state_init, double vxy_min, double vz_max, double roll_rate, double roll_max, double acc_xy, double acc_z) const {
    double time0(state_init.Time());
    const double yaw0 = state_init.Heading();
    double yaw=yaw0;
    double vx0 = state_init.SpeedXY();
    double vx (vx0); 
    double x = state_init.Pos()[0];
    double y = state_init.Pos()[1];
    double roll = state_init.Roll();
    // prevent discotinious traj
    if (std::abs(roll) > roll_max) {
        roll_max = std::abs(roll);
    }
    double w ;
    double R ;
    GetRadiusAndOmega(R,w,vx,roll);
    std::vector<double> x_val;
    std::vector<double> y_val;
    std::vector<double> h_val;
    std::vector<double> vxy_val;
    std::vector<double> vh_val;
    std::vector<double> times;
    double time (0); //corrected at the end (time0);
    while (true) {    
        x_val.push_back(x);
        y_val.push_back(y);
        vxy_val.push_back(vx);
        h_val.push_back(yaw);
        vh_val.push_back(w); // omega is head rate
        times.push_back(time);
        /* if we achieved end of transition trajectory */
        // WRAP YAW -pi pi
        if (std::abs(roll) == roll_max && std::abs(yaw0-yaw) >= M_PI) {
            break;            
        }
        /* change roll angle*/    
        roll += roll_rate *time_res_;
        if (std::abs(roll) > roll_max) {
            roll = roll < 0 ? -roll_max : roll_max;
        }    
        /* change velocity */
        vx = vx0 + acc_xy*time;
        if (vx < vxy_min) {
            vx=vxy_min;
        }
        GetRadiusAndOmega(R,w,vx,roll);
        yaw = yaw + w*time_res_;
        x = x+ (vx*time_res_)*cos(yaw);
        y = y+ (vx*time_res_)*sin(yaw);
        time +=time_res_;
    }

    //------------------------Z is independent of x y dynamics-------------//
    std::vector<double> z_val;
    std::vector<double> vz_val;
    GetZEmergencyTrajectory(x_val.size(), state_init.Pos()[2],  state_init.SpeedZ(), vz_max, acc_z, z_val, vz_val);
    
    Trajectory<StateFW> traj;
    for (size_t i(0); i<x_val.size();++i) {
        StateFW state(x_val[i],y_val[i],z_val[i],h_val[i],vxy_val[i],vz_val[i],vh_val[i],times[i]+time0);
        traj.Append(state);
    }
    bool clock_wise = roll_rate > 0 ? true : false;
    Loiter loit0(traj.Back(),clock_wise);
    return std::make_pair(traj,loit0);
}

//TODO do we need 0.0 head_rate at the final point?
std::pair<Trajectory<StateFW>,Loiter> OpenLoopEmergencyManeuver::
GetSlowSpeedEmergencyTrajectory(const StateFW &state_init, double vxy_min, double vz_max, double head_rate_acc, double head_rate_max,  double acc_xy, double acc_z) const {
    double time0(state_init.Time());
    const double yaw0 = state_init.Heading();
    double yaw=yaw0;
    double vx0 = state_init.SpeedXY();
    double vx (vx0); 
    double w(state_init.HeadRate());
    double x = state_init.Pos()[0];
    double y = state_init.Pos()[1];
    // in order to prevent discontinious
    if (std::abs(w) > head_rate_max) {
        head_rate_max = std::abs(w);
    }
    std::vector<double> x_val;
    std::vector<double> y_val;
    std::vector<double> h_val;
    std::vector<double> vxy_val;
    std::vector<double> vh_val;
    std::vector<double> times;
    double time (0); //corrected at the end (time0);
    while (true) {    
        x_val.push_back(x);
        y_val.push_back(y);
        vxy_val.push_back(vx);
        h_val.push_back(yaw);
        vh_val.push_back(w); // omega is head rate
        times.push_back(time);
        /* if we achieved end of transition trajectory */
        // WRAP YAW -pi pi
        if (vx==vxy_min){//(std::abs(yaw0-yaw) >= M_PI) {
            break;            
        }
        /* change velocity */
        vx = vx0 + acc_xy*time;
        if (vx < vxy_min) {
            vx=vxy_min;
        }
        
        /* change head rate */
        w += head_rate_acc*time_res_;
        if (std::abs(w) > head_rate_max) {
            w = w < 0 ? -head_rate_max : head_rate_max;
        }    

        yaw = yaw + w*time_res_;
        x = x+ (vx*time_res_)*cos(yaw);
        y = y+ (vx*time_res_)*sin(yaw);
        time +=time_res_;
    }

    //------------------------Z is independent of x y dynamics-------------//
    std::vector<double> z_val;
    std::vector<double> vz_val;
    GetZEmergencyTrajectory(x_val.size(), state_init.Pos()[2],  state_init.SpeedZ(), vz_max, acc_z, z_val, vz_val);
    
    Trajectory<StateFW> traj;
    for (size_t i(0); i<x_val.size();++i) {
        StateFW state(x_val[i],y_val[i],z_val[i],h_val[i],vxy_val[i],vz_val[i],vh_val[i],times[i]+time0);
        traj.Append(state);
    }
    bool clock_wise = head_rate_acc > 0 ? false : true;
    Loiter loit0(traj.Back(),clock_wise);
    return std::make_pair(traj,loit0);
}

void OpenLoopEmergencyManeuver::
GetZEmergencyTrajectory(size_t num_pts, double z0, double vz0, double vz_max, double acc_z, std::vector<double> &z_val, std::vector<double> &vz_val, bool minimize_height_deviation) const {
    double vz (vz0);
    double z (z0);
    double max_time (time_res_*(num_pts-1)); // -1 first point constant
    size_t num_acc, num_dcc;
    double dcc_z;
    if (acc_z < 0 ){ 
        vz_max = -vz_max;
    }
    if (max_time > (vz_max/acc_z + (vz_max-vz0)/acc_z)) {
        double z_acc_time = (vz_max - vz0)/acc_z;
        double z_dcc_time = vz_max/acc_z;
        num_acc = std::ceil(z_acc_time/time_res_);
        num_dcc = std::ceil(z_dcc_time/time_res_);
        dcc_z = (vz_max)/(num_dcc*time_res_);
    }
    else {
        double time_tmp = max_time - std::abs(vz0/acc_z);
        double z_acc_time, z_dcc_time;
        if ((acc_z*vz0) >0) { // same sign
            z_acc_time = time_tmp/2.0;
            z_dcc_time = time_tmp/2.0 + std::abs(vz0/acc_z);
        }
        else{
            z_acc_time = time_tmp/2.0 + std::abs(vz0/acc_z);
            z_dcc_time = time_tmp/2.0;
        }
        num_acc = std::floor(z_acc_time/time_res_);
        num_dcc = std::ceil(z_dcc_time/time_res_);
        dcc_z = (vz0+num_acc*acc_z*time_res_)/(num_dcc*time_res_);    
    }

    // If desired behavior is to level as soon as possible, decelerate and maintain zero speed
    if(minimize_height_deviation)
    {
      num_dcc = 0;
      dcc_z = 0;
      num_acc = ceil(vz0 / fabs(acc_z));
      acc_z = (0 - vz0) / (double) (num_acc);
    }

    // first point
    vz_val.push_back(vz);
    z_val.push_back(z);
    double time (time_res_);
    //for(size_t i(1); i<x_val.size(); ++i) {
    for(size_t i(1); i<num_pts; ++i) {
        if (i<=num_acc) {
            vz = vz + acc_z*time_res_;
        }
        else if (i>=num_pts-num_dcc) {
            vz = vz - dcc_z*time_res_;
        }
        else{ 
            // vz is constant
        }
        // saturate speed
        if (std::abs(vz) > std::abs(vz_max)) {
            vz = vz_max;// sign already correct
        }    
        // position
        z =z+vz*time_res_;
        time += time_res_;
        vz_val.push_back(vz);
        z_val.push_back(z);
    }

    // vel of last point should be very small vel < 1e-10, set it hard to zero
    vz_val.back()=0.0;
}


std::vector<std::pair<Trajectory<StateFW>,Loiter> > OpenLoopEmergencyManeuver::
GetEmergencyWindTrajectories(const StateFW &state) const {
    CA_ERROR("not implemented yet!");
    return std::vector<std::pair<Trajectory<StateFW>,Loiter> >();
}

std::pair<Trajectory<StateFW>,Loiter> OpenLoopEmergencyManeuver::
GetEmergencyWindTrajectory(const StateFW &state, double max_roll, double acc_z) const {
    CA_ERROR("not implemented yet!");
    return std::make_pair(Trajectory<StateFW>(),Loiter(state,true));
}

std::pair<Trajectory<StateFW>, SmoothLoiter> OpenLoopEmergencyManeuver::
GetSmoothHighSpeedEmergencyTrajectory(
	const StateFW &state_init, double vxy_min, double vz_max,
	double roll_rate, double roll_max, double acc_xy, double acc_z, bool minimize_height_deviation) const {

	double time0(state_init.Time());
	const double yaw0 = state_init.Heading();
	double yaw = yaw0;
	double vx0 = state_init.SpeedXY();
	double vx (vx0);
	double x = state_init.Pos()[0];
	double y = state_init.Pos()[1];
	double roll = state_init.Roll();
	double direction = vx0 >  vxy_min ? -1.0 : 1.0;

	// prevent discontinuous traj - this is retarded...check this. TODO
//	if (std::abs(roll) > roll_max) {
//		roll_max = std::abs(roll);
//	}
	double w ;
	double R ;
	GetRadiusAndOmega(R, w, vx, roll);
	std::vector<double> x_val;
	std::vector<double> y_val;
	std::vector<double> h_val;
	std::vector<double> vxy_val;
	std::vector<double> vh_val;
	std::vector<double> times;
	double time (0); //corrected at the end (time0);

//	bool clock_wise = roll_rate > 0 ? false : true;
	while (true) {
		// Build up state
		x_val.push_back(x);
		y_val.push_back(y);
		vxy_val.push_back(vx);
		h_val.push_back(yaw);
		vh_val.push_back(w); // omega is head rate
		times.push_back(time);

		/* change velocity */
		vx = vx0 + direction*fabs(acc_xy)*time;
		if (direction * (vx - vxy_min) > 0) {
			vx = vxy_min;
			break;
			// Check if our bank matches the direction of our turn
//			if(!clock_wise) {
//				// roll must be positive
//				if(roll > 0) break;
//			}
//			else if(roll < 0) break;
		}

		/* change roll angle*/
		roll += roll_rate * time_res_;
		if (std::abs(roll) > roll_max) {
			roll = roll < 0 ? -roll_max : roll_max;
		}

		GetRadiusAndOmega(R,w,vx,roll);
		yaw = math_utils::angular_math::WrapTo2Pi(yaw + -w*time_res_);
//		yaw = yaw + w * time_res_;
		x = x + (vx*time_res_) * cos(yaw);
		y = y + (vx*time_res_) * sin(yaw);
		time += time_res_;
	}

	//------------------------Z is independent of x y dynamics-------------//
	std::vector<double> z_val;
	std::vector<double> vz_val;
	GetZEmergencyTrajectory(x_val.size(), state_init.Pos()[2],  state_init.SpeedZ(), vz_max, acc_z, z_val, vz_val, minimize_height_deviation);

	Trajectory<StateFW> traj;
	for (size_t i(0); i<x_val.size();++i) {
		StateFW state(x_val[i], y_val[i], z_val[i],
				h_val[i], vxy_val[i], vz_val[i],
				vh_val[i], times[i]+time0);
		traj.Append(state);
	}

	bool clock_wise = (roll < 0);
	SmoothLoiter loit0(traj.Back(), roll, clock_wise, clk_em_, cclk_em_, state_init.Heading());
//	std::cout << traj.Back() << "\n";
	return std::make_pair(traj,loit0);
}

std::pair<Trajectory<StateFW>, SmoothLoiter> OpenLoopEmergencyManeuver::
GetSmoothSlowSpeedEmergencyTrajectory(
    const StateFW &state_init, double vxy_min, double vz_max,
    double head_rate_acc, double head_rate_max, double acc_xy,
    double acc_z) const {

    double time0(state_init.Time());
    const double yaw0 = state_init.Heading();
    double yaw=yaw0;
    double vx0 = state_init.SpeedXY();
    double vx (vx0);
    double w(state_init.HeadRate());
    double x = state_init.Pos()[0];
    double y = state_init.Pos()[1];
    // in order to prevent discontinious
    if (std::abs(w) > head_rate_max) {
        head_rate_max = std::abs(w);
    }
    std::vector<double> x_val;
    std::vector<double> y_val;
    std::vector<double> h_val;
    std::vector<double> vxy_val;
    std::vector<double> vh_val;
    std::vector<double> times;
    double time (0); //corrected at the end (time0);
    while (true) {
        x_val.push_back(x);
        y_val.push_back(y);
        vxy_val.push_back(vx);
        h_val.push_back(yaw);
        vh_val.push_back(w); // omega is head rate
        times.push_back(time);
        /* if we achieved end of transition trajectory */
        // WRAP YAW -pi pi
        if (vx==vxy_min){//(std::abs(yaw0-yaw) >= M_PI) {
            break;
        }
        /* change velocity */
        vx = vx0 + acc_xy*time;
        if (vx < vxy_min) {
            vx=vxy_min;
        }

        /* change head rate */
        w += head_rate_acc*time_res_;
        if (std::abs(w) > head_rate_max) {
            w = w < 0 ? -head_rate_max : head_rate_max;
        }

        yaw = yaw + w*time_res_;
        x = x+ (vx*time_res_)*cos(yaw);
        y = y+ (vx*time_res_)*sin(yaw);
        time +=time_res_;
    }

    //------------------------Z is independent of x y dynamics-------------//
    std::vector<double> z_val;
    std::vector<double> vz_val;
    GetZEmergencyTrajectory(x_val.size(), state_init.Pos()[2],  state_init.SpeedZ(), vz_max, acc_z, z_val, vz_val);

    Trajectory<StateFW> traj;
    for (size_t i(0); i<x_val.size();++i) {
        StateFW state(x_val[i],y_val[i],z_val[i],h_val[i],vxy_val[i],vz_val[i],vh_val[i],times[i]+time0);
        traj.Append(state);
    }
    bool clock_wise = head_rate_acc > 0 ? false : true;
    SmoothLoiter loit0(traj.Back(), 0, clock_wise, clk_em_, cclk_em_, state_init.Heading());
    return std::make_pair(traj,loit0);
}


// also from guaranteeSafePlanning package
void OpenLoopEmergencyManeuver::
GetRadiusAndOmega(double &radius,double &w, double velocity, double roll) const {
    if( roll == 0) {
        radius = 0.0;
        w =0.0;
    }
    else  {
        radius = abs( (velocity*velocity)/(g*tan(roll)) );
        w = roll < 0 ? velocity/radius : -velocity/radius;
    }
};

