/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/*
 * loiter_smooth.cpp
 *
 *  Created on: Apr 23, 2016
 *      Author: vdugar
 */

#include "emergency_maneuver_library/trajectory.h"
#include <Eigen/Dense>
#include "emergency_maneuver_library/loiter_smooth.h"
#include <math.h>
#include "math_utils/math_utils.h"
#include <fstream>
#include <ros/console.h>
#include "exception/exception.h"

namespace ca {
namespace emergency_maneuver_library {

//Eigen::MatrixXd GetSmoothClkLoiter() {
//	int r(435), c(13);
//	Eigen::MatrixXd data(r, c);
//
//	std::ifstream fin("/home/vdugar/work/smooth_eml_matlab/clockwise.txt");
//
//	double d;
//	for(int i(0); i < r; ++i) {
//		for(int j(0); j < c; ++j) {
//			fin >> d;
//			data(i, j) = d;
//		}
//	}
//
//	return data;
//}
//
//Eigen::MatrixXd GetSmoothCClkLoiter() {
//	int r(435), c(13);
//	Eigen::MatrixXd data(r, c);
//
//	std::ifstream fin("/home/vdugar/work/smooth_eml_matlab/counterclock.txt");
//
//	double d;
//	for(int i(0); i < r; ++i) {
//		for(int j(0); j < c; ++j) {
//			fin >> d;
//			data(i, j) = d;
//		}
//	}
//
//	return data;
//}

//Eigen::MatrixXd GetSmoothLoiterFromFile(std::string fn) {
//	int r(435), c(13);
//	Eigen::MatrixXd data(r, c);
//
//	std::ifstream fin(fn);
//
//	double d;
//	for(int i(0); i < r; ++i) {
//		for(int j(0); j < c; ++j) {
//			fin >> d;
//			data(i, j) = d;
//		}
//	}
//
//	return data;
//}

Eigen::Matrix3d SmoothLoiter::
GetRotationMatrix(double angle) {
	Eigen::Matrix3d rot(3, 3);
	rot << cos(angle), sin(angle), 0,
		 -sin(angle), cos(angle), 0,
		 0, 0, 1;

	return rot;
}

double SmoothLoiter::
HeadingDiff(double th1, double th2) {
	double head_change = std::abs(th2-th1);
	if(head_change > M_PI) {
		head_change = 2 * M_PI - head_change;
	}

	Eigen::Vector3d v1(cos(th1), sin(th1), 0);
	Eigen::Vector3d v2(cos(th2), sin(th2), 0);
	Eigen::Vector3d temp = v1.cross(v2);
	int sign = temp(2) > 0 ? 1 : -1;

	return (head_change * sign);
}

Eigen::MatrixXd SmoothLoiter::
GetLoiter() {
	if(clockwise_)
		return clk_em_;
	else
		return cclk_em_;
}

unsigned int SmoothLoiter::
MatchBank(Eigen::MatrixXd& loit) {
	double best_alignment = 1e5;
	unsigned int best_align_idx = -1;
//	ROS_INFO("vdgr: Matching bank for %lf", bank_init_);
	for(int i(0); i < loit.rows(); ++i) {
		if(std::abs(bank_init_ - loit(i, 5)) < 1e-2) {
//			return i;
			// check for alignment
			double rot_amt = HeadingDiff(loit(i, 3), head_init_);
			double head_diff = HeadingDiff(em_init_bank_,
					 math_utils::angular_math::WrapTo2Pi(rot_amt));
			if(std::abs(head_diff) < best_alignment) {
				best_alignment = std::abs(head_diff);
				best_align_idx = i;
			}
		}
	}

	return best_align_idx;
}

/*
 * Checks if pt lies between idx1 and idx2, where idx2 = idx1 + step.
 * Assumed a circular list of size n
 */
bool CheckCrossover(int idx1, int idx2, int pt, int step, unsigned int n) {
	int diff1 = std::abs(idx1 - pt);
	diff1 = (diff1 > n/2) ? (n-diff1) : diff1;
	int diff2 = std::abs(idx2 - pt);
	diff2 = (diff2 > n/2) ? (n-diff2) : diff2;

	if(diff1 + diff2 == step)
		return true;
	else return false;
}

Trajectory<StateFW> SmoothLoiter::
GetClosedTrajectory(double time_res) {
	Trajectory<StateFW> traj;

	double traj_time_res = 0.1;

	// Check if we should hover
	if(airspeed_ < 1e-5) {
		// repeat the same state with different times
//		ROS_INFO("vdgr: slow speed maneuver requested");
		double time(time0_);
		// headrate and speedz 0 by definition of hover
		StateFW state(Eigen::Vector3d(x_, y_, z_), head_init_, airspeed_, 0.0, 0.0, time);
		traj.Append(state);
		state.Time(time + time_res);
		traj.Append(state);
		ROS_INFO_THROTTLE(1.0, "vdgr: slow speed man returned with time and time res %lf, %lf: ", time+time_res, time_res);
		return traj;
	}

	// Get index step, depending on time res
//	ROS_INFO("vdgr: time res = %lf, time0 = %lf ", time_res, time0_);
	int index_step = (int) (time_res / traj_time_res);
//	ROS_INFO("vdgr: index step = %d yay", index_step);

	// Get loiter depending on initial conditions (=clk/cclk turn for now)
	Eigen::MatrixXd loit = GetLoiter();
//	ROS_INFO("vdgr: Got loiter with direction %d", clockwise_);

	// Get point along loiter where the bank is matched
	unsigned int index = MatchBank(loit);
	if (index == -1) {
	    return traj;
	}
//	ROS_INFO("vdgr: Bank matched at %d with %lf ", index, loit(index, 5));

	// Get rotation matrix to line up loiter with the initial heading
	double head_diff = HeadingDiff(loit(index, 3), head_init_);
	Eigen::Matrix3d rot = GetRotationMatrix(-head_diff);
//	ROS_INFO("vdgr: Got rotation matrix for head_diff %lf with init_head %lf", head_diff, head_init_ );

	// Construct trajectory
	unsigned int num_points = loit.rows();
	unsigned int start = (index + index_step) % num_points;
	unsigned int i = start;
	unsigned int count = 0;
	Eigen::Vector3d temp_pos;
	double temp_head;

	// Get displacement vector
	temp_pos << loit(i, 0), loit(i, 1), loit(i, 2);
	temp_pos = rot * temp_pos;
	Eigen::Vector3d new_origin; new_origin << x_, y_, z_;
	Eigen::Vector3d disp_vector = new_origin - temp_pos;

	bool looped = false;
	int prev_i = -1;
	while( true ) {
		++count;

		// Rotate X-Y-Z point to match initial heading
		temp_pos << loit(i, 0), loit(i, 1), loit(i, 2);
		temp_pos = rot * temp_pos;

		// Adjust heading
//		temp_head = math_utils::angular_math::WrapTo2Pi(loit(i, 3) + head_diff);
		temp_head = loit(i, 3) + head_diff;

		// Generate state
		StateFW state(temp_pos + disp_vector,
				temp_head, airspeed_, 0.0,
				-loit(i, 9), time0_ + count*time_res);
		traj.Append(state);

		// check if we've looped around the trajectory
//		if(i+index_step >= num_points) looped = true;

		i = (i + index_step) % num_points;
		if(CheckCrossover(prev_i, i, start, index_step, num_points)) {
//			ROS_INFO("vdgr: Broken after count %d with time %lf", count, time0_ + count*time_res);
			break;
		}
		prev_i = i;

//		ROS_INFO("vdgr: In loop with count %d", count);
	}

	// Append last point ( = first point)
	StateFW state = traj.Front();
	int diff = std::abs(i - start);
	diff = (diff > num_points/2) ? (num_points-diff) : diff;
	double time = traj.Back().Time() + traj_time_res * (index_step - diff);
	state.Time(time);
//	ROS_INFO("vdgr: Done looping with prev_time %lf and full-time %lf", traj.Back().Time(), time);

	traj.Append(state);

	return traj;
}


std::vector<Eigen::Vector3d> SmoothLoiter::GetClosedTrajectoryPoints(double time_res) {
    std::vector<Eigen::Vector3d> traj_pts;

	double traj_time_res = 1.0;

	// Check if we should hover
	if(airspeed_ < 1e-5) {
		// repeat the same state with different times
		Eigen::Vector3d pnt(x_, y_, z_);
		traj_pts.push_back(pnt);
        traj_pts.push_back(pnt);
		return traj_pts;
	}

	// Get index step, depending on time res
	int index_step = (int) (time_res / traj_time_res);

	// Get loiter depending on initial conditions (=clk/cclk turn for now)
	Eigen::MatrixXd loit = GetLoiter();

	// Get point along loiter where the bank is matched
	unsigned int index = MatchBank(loit);
	if (index == -1) {
	    return traj_pts;
	}

	// Get rotation matrix to line up loiter with the initial heading
	double head_diff = HeadingDiff(loit(index, 3), head_init_);
	Eigen::Matrix3d rot = GetRotationMatrix(-head_diff);

	// Construct trajectory
	unsigned int num_points = loit.rows();
	unsigned int start = (index + index_step) % num_points;
	unsigned int i = start;
	unsigned int count = 0;
	Eigen::Vector3d temp_pos;
	double temp_head;

	// Get displacement vector
	temp_pos << loit(i, 0), loit(i, 1), loit(i, 2);
	temp_pos = rot * temp_pos;
	Eigen::Vector3d new_origin; new_origin << x_, y_, z_;
	Eigen::Vector3d disp_vector = new_origin - temp_pos;

	bool looped = false;
	int prev_i = -1;
	while( true ) {
		++count;

		// Rotate X-Y-Z point to match initial heading
		temp_pos << loit(i, 0), loit(i, 1), loit(i, 2);
		temp_pos = rot * temp_pos;

		// Adjust heading
		temp_head = loit(i, 3) + head_diff;

		// Generate state
		Eigen::Vector3d pnt = temp_pos + disp_vector;
		traj_pts.push_back(pnt);

		i = (i + index_step) % num_points;
		if(CheckCrossover(prev_i, i, start, index_step, num_points)) {
			break;
		}
		prev_i = i;
	}

	// Append last point ( = first point)
	Eigen::Vector3d aux = *traj_pts.begin();
	traj_pts.push_back(aux);

	return traj_pts;
}


} // emergency_maneuver_library
} // ca
