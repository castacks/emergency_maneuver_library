function [maneuvers] = get_high_speed_emergency_trajectories(x0,y0,z0,vx0,vz0,head0,roll0,roll_rate,roll_max,roll_max_res,acc_xy,acc_z_max,acc_z_max_res,v_min,vz_max,time0,time_res)

num_zval = floor(acc_z_max/acc_z_max_res);
maneuvers=[];
for i=0:2*num_zval
	acc_z = (-num_zval+i) *acc_z_max_res;
	if vz0~= 0 && acc_z == 0 % we cant reach level
		continue;
	end
	%for max_roll = 0 : roll_max_res : roll_max
	for max_roll = roll_max_res : roll_max_res : roll_max
		if length(maneuvers) == 11
			wait = 1;
		end
		[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,max_roll,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res);
		tmp_man.traj = transition_traj;
		tmp_man.loiter = loiter;
		maneuvers = [maneuvers,tmp_man];
		if max_roll ~= 0
			[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,max_roll,-roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res);
			tmp_man.traj = transition_traj;
			tmp_man.loiter = loiter;
			maneuvers = [maneuvers,tmp_man];	
		end
	end
end





