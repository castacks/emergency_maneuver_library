clear;
close all;
clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x0=100;
y0=0;
z0=0;
%vx0=50;
vz0=0;
head0=0;
roll0=0;
roll_max = 30*pi/180;
roll_rate = 15*pi/180;
acc_xy = -1 * 0.1 *9.81;
acc_z = 0;
v_min = 20;
vz_max =0.0;
time0 = 0;
time_res = 0.1;

figure;hold on;
axis equal;

vx0=60
[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res)
plot(transition_traj(1,:),transition_traj(2,:),'k');
[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,-roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res)
plot(transition_traj(1,:),transition_traj(2,:),'k');

vx0=55
[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res)
plot(transition_traj(1,:),transition_traj(2,:),'b');
[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,-roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res)
plot(transition_traj(1,:),transition_traj(2,:),'b');

vx0=50
[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res)
plot(transition_traj(1,:),transition_traj(2,:),'m');
[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,-roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res)
plot(transition_traj(1,:),transition_traj(2,:),'m');
vx0=45
[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res)
plot(transition_traj(1,:),transition_traj(2,:),'g');
[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,-roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res)
plot(transition_traj(1,:),transition_traj(2,:),'g');
vx0=40
[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res)
plot(transition_traj(1,:),transition_traj(2,:),'r');
[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,-roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res)
plot(transition_traj(1,:),transition_traj(2,:),'r');

%% draw laser fov
phi = -60*pi/180:0.01:40*pi/180;
radius = 750;
x=radius*cos(phi);
y=radius*sin(phi);
plot(x,y,'k');
plot([0,x(1)],[0,y(1)],'k');
plot([0,x(end)],[0,y(end)],'k');

legend('60m/s','60m/s','55m/s','55m/s','50m/s','50m/s','45m/s','45m/s','40m/s','40m/s');
xlabel('x[m]');
ylabel('y[m]');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


x0= 0;
y0= 0;
z0= 0;
vx0=40;
vz0=0;
roll0=0.0;
roll_max= 25*pi/180.0;
roll_rate= -0.25;
acc_xy = -0.7;
acc_z = +0.7;
v_min=20.0;
time_res=0.1;
vz_max = 5.0;
head0=0.0;
time0=0.0;

[traj_trans,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res);
traj_loit = get_traj_circle(traj_trans(1,end),traj_trans(2,end),traj_trans(3,end),traj_trans(4,end),traj_trans(7,end),traj_trans(5,end),traj_trans(8,end),loiter,time_res);

m_to_nautical = 0.000539957; % m to nautical mile
m_to_feet = 3.28084;
m_to_1000_feet = 0.00328084;
ms_to_knot = 1.94384449;
ms_to_feetmin = 196.850394;


set(0,'defaultaxeslinewidth',2)
set(0,'defaultpatchlinewidth',2)


figure;
hold on; axis equal;axis tight;

set(findall(gcf,'-property','FontSize'),'FontSize',15) 
traj=[traj_trans,traj_loit];%traj_loit(:,1:end-50)];
traj(1,:) = traj(1,:) .* m_to_nautical;
traj(2,:) = traj(2,:) .* m_to_nautical;
traj(3,:) = traj(3,:) .* m_to_feet;
plot3(traj(1,:),traj(2,:),traj(3,:),'LineWidth',2.0,'LineSmoothing','on');

xlabel('x [NM]');
ylabel('y [NM]');
zlabel('z [feet]');

figure; hold on; %axis equal;
set(findall(gcf,'-property','FontSize'),'FontSize',15) 
xlabel('t [s]');
ylabel('z [feet]');
plot(traj(end,:),traj(3,:),'LineWidth',2.0,'LineSmoothing','on');

figure; hold on; 
set(findall(gcf,'-property','FontSize'),'FontSize',15) 
xlabel('t [s]');
ylabel('speed [knot]');
% for i=1:length(traj)
%     speed(i) = norm([traj(5,i),traj(6,i)]) * ms_to_knot;
% end
% plot(traj(end,:),speed);
plot(traj(end,:),traj(5,:).*ms_to_knot,'LineWidth',2.0,'LineSmoothing','on');

figure; hold on; 
set(findall(gcf,'-property','FontSize'),'FontSize',15) 
xlabel('t [s]');
ylabel('vel z [feet/min]');
plot(traj(end,:),traj(6,:).*ms_to_feetmin,'LineWidth',2.0,'LineSmoothing','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


x0=10;
y0=-20;
z0=-40;
vx0=50;
vz0=-6;
roll0=0.0;
roll_max= 0.5;
roll_rate= -0.25;
acc_xy = -0.8;
acc_z = +0.9;
v_min=20.0;
time_res=0.1;
vz_max = 7.0;
head0=0.0;
time0=0.0;

[traj_trans,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res);
traj_loit = get_traj_circle(traj_trans(1,end),traj_trans(2,end),traj_trans(3,end),traj_trans(4,end),traj_trans(7,end),traj_trans(5,end),traj_trans(8,end),loiter,time_res);

figure;
hold on; axis equal;
traj=[traj_trans,traj_loit(:,1:end-50)];
plot3(traj(1,:),traj(2,:),traj(3,:));
traj_trans(6,end-10:end)
plot(loiter.center(1),loiter.center(2),'r*');
figure;plot(traj_trans(6,:));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


x0=-10;
y0=20;
z0=40;
vx0=50;
vz0=2;
roll0=-0.2002;
roll_max= 0.5;
roll_rate= +0.2;
acc_xy = -0.8;
acc_z = -0.9;
v_min=20.0;
time_res=0.1;
vz_max = 20.0;
head0=-2/3*pi;
time0=5.0;

[traj_trans,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res);
traj_loit = get_traj_circle(traj_trans(1,end),traj_trans(2,end),traj_trans(3,end),traj_trans(4,end),traj_trans(7,end),traj_trans(5,end),traj_trans(8,end),loiter,time_res);

figure;
hold on; axis equal;
traj=[traj_trans,traj_loit(:,1:end-50)];
plot3(traj(1,:),traj(2,:),traj(3,:));
traj_trans(6,end-10:end)
plot(loiter.center(1),loiter.center(2),'r*');
figure;plot(traj_trans(6,:));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


x0=-10;
y0=-20;
z0=5;
vx0=20;
vz0=-1;
roll0=-0.2;
roll_max= 0.5;
roll_rate= +0.2;
acc_xy = -0.8;
acc_z = +0.3;
v_min=0.0;
time_res=0.1;
vz_max = 3.0;
head0=+2/3*pi;
time0=8.0;
head_rate0 = -0.05;
head_rate = -0.01;
head_rate_max = 0.1;
[traj_trans_slow,loiter_slow] = generate_slow_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,head_rate0,head_rate,head_rate_max,acc_xy,acc_z,v_min,vz_max,time0,time_res);
traj_loit_slow = get_traj_circle(traj_trans_slow(1,end),traj_trans_slow(2,end),traj_trans_slow(3,end),traj_trans_slow(4,end),traj_trans_slow(7,end),traj_trans_slow(5,end),traj_trans_slow(8,end),loiter_slow,time_res);


figure;
hold on; axis equal;
traj=[traj_trans_slow,traj_loit_slow(:,1:end-50)];
plot3(traj(1,:),traj(2,:),traj(3,:));
traj_trans_slow(6,end-10:end)
figure;plot(traj_trans_slow(6,:));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


x0=-10;
y0=-20;
z0=5;
vx0=20;
vz0=-1;
roll0=-0.2;
roll_max= 0.5;
roll_rate= +0.2;
acc_xy = -0.8;
acc_z = 0.06;%+0.3;
v_min=0.0;
time_res=0.1;
vz_max = 3.0;
head0=+2/3*pi;
time0=8.0;
head_rate0 = -0.05;
head_rate_acc = -0.01;
head_rate_max = 0.0;%0.1;
[traj_trans_slow,loiter_slow] = generate_slow_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,head_rate0,head_rate_acc,head_rate_max,acc_xy,acc_z,v_min,vz_max,time0,time_res);
traj_loit_slow = get_traj_circle(traj_trans_slow(1,end),traj_trans_slow(2,end),traj_trans_slow(3,end),traj_trans_slow(4,end),traj_trans_slow(7,end),traj_trans_slow(5,end),traj_trans_slow(8,end),loiter_slow,time_res);


figure;
hold on; axis equal;
traj=[traj_trans_slow,traj_loit_slow(:,1:end-50)];
plot3(traj(1,:),traj(2,:),traj(3,:));
traj_trans_slow(6,end-10:end)
figure;plot(traj_trans_slow(6,:));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x0=25;
y0=100;
z0=-20;
vx0=15;
vz0=+2;
acc_xy = -0.5;
acc_z_max = 0.5;
acc_z_max_res = 0.1;
v_min=0.0;
time_res=0.2;
vz_max = 10;
head0=pi;
time0=13;
head_rate0 = +0.04;
head_rate_max = 0.2;
head_rate_max_res = 0.05;
head_rate_acc = 0.03;

maneuvers = get_slow_speed_emergency_trajectories(x0,y0,z0,vx0,vz0,head0,head_rate0,head_rate_acc,head_rate_max,head_rate_max_res,acc_xy,acc_z_max,acc_z_max_res,v_min,vz_max,time0,time_res);
figure; hold all;
for i=1:length(maneuvers)
	plot3(maneuvers(i).traj(1,:),maneuvers(i).traj(2,:),maneuvers(i).traj(3,:));
end
num_man = length(maneuvers);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x0=25;
y0=100;
z0=-20;
vx0=44.5;
vz0=+2;
acc_xy = -0.5;
acc_z_max = 0.5;
acc_z_max_res = 0.1;
v_min=20.0;
time_res=0.2;
vz_max = 10;
head0=-pi;
roll0=+0.05;
roll_max= 0.5;
roll_max_res = 0.1;
roll_rate= +0.2;
time0=13;

maneuvers = get_high_speed_emergency_trajectories(x0,y0,z0,vx0,vz0,head0,roll0,roll_rate,roll_max,roll_max_res,acc_xy,acc_z_max,acc_z_max_res,v_min,vz_max,time0,time_res);
figure; hold all;
for i=1:length(maneuvers)
	plot3(maneuvers(i).traj(1,:),maneuvers(i).traj(2,:),maneuvers(i).traj(3,:)); 
end
num_man = length(maneuvers);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% discovered problem with state [1138.15,-3235.31,-611.57,2.25538,29.9982,-1.07837e-11,-2.90276,16195] in C++

x0=1138.5;
y0=-3235.31;
z0=-611.57;
head0=2.25538;
vx0=29.9982;
vz0=-1.07837e-11;
head_rate0 = -2.90276;
roll0= -atan( (vx0/9.81) * head_rate0);
time0=16195;
v_min = 20.0;
time_res = 0.1;
acc_z_max=0.8;
acc_z_max_res = 0.8;
vel_z_max = 5.0;
acc_xy=-0.9;
roll_max=0.5;
roll_max_res=0.25;
roll_rate = 0.25;
maneuvers = get_high_speed_emergency_trajectories(x0,y0,z0,vx0,vz0,head0,roll0,roll_rate,roll_max,roll_max_res,acc_xy,acc_z_max,acc_z_max_res,v_min,vz_max,time0,time_res);
figure; hold all;
for i=1:length(maneuvers)
	plot3(maneuvers(i).traj(1,:),maneuvers(i).traj(2,:),maneuvers(i).traj(3,:)); 
end
num_man = length(maneuvers);


%% only loiter test
x0=174.012;
y0=297.207;
z0=20.0;
h0=2.3708;
h0_dot=0.1;
v0=30.0;
t0=0.4;
time_res=0.1;
loiter.radius=300;
loiter.center = [-34.9992, 81.9992,20];
loiter.clock_wise=false;
traj_loiter = get_traj_circle(x0,y0,z0,h0,h0_dot,v0,t0,loiter,time_res);


