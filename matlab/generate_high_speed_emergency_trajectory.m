function [transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,roll0,roll_max,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res)
%PLOTCHANGINGCURVATURETRAJECTORY Summary of this function goes here
%   Detailed explanation goes here
g = 9.80665;
v = vx0;
a = acc_xy;
az = acc_z;
roll=roll0;
vz=vz0;

if (abs(roll)>roll_max) 
	roll_max = abs(roll);
end

x = x0;
y = y0;
theta = head0;
z = z0;
%% init heading rate
if roll ~= 0 && v ~=0
	R = abs((v.*v)/(g*tan(roll)));
	w = v/R * sign(-roll);
else
	R = 0;
	w  = 0;
end    
X=[];
Y=[];
Z=[];
Heading=[];
Heading_rate=[];
Roll=[];
time=0;% corrected at the endtime0;
V_XY=[];
V_Z=[];
Time=[];
while true
    X(end+1) = x;
    Y(end+1) = y;
    Heading(end+1) = theta;
    Heading_rate(end+1) = w;
    Roll(end+1) = roll;
    V_XY(end+1)=v;
    Time(end+1)=time;
            
	if abs(roll) == roll_max && abs(head0-theta) >= pi
		if roll_rate < 0 % left turn
            z_vec = [0;0;-1];
        else
            z_vec = [0;0;+1];
        end
        mot_dir = [cos(theta)*v;sin(theta)*v];
        mot_dir = mot_dir./norm(mot_dir);
        dir_to_center = cross([mot_dir;0],z_vec);
        center = dir_to_center(1:2)*abs(R) + [x;y];
        loiter.center = center;
        loiter.radius = abs(R);
        loiter.speed = v;
		if roll_rate > 0
			loiter.clock_wise = true;
		else
			loiter.clock_wise = false;
		end
		break;
	end
	
	
    roll = roll + roll_rate*time_res;
    if abs(roll) > roll_max
        roll = roll_max *sign(roll);
    end
    v = vx0 + a*time;
    if v< v_min
        v=v_min;
	end
    
	%TODO w also negative?
    if roll ~= 0 && v ~=0
        R = abs((v.*v)/(g*tan(roll)));
        w = v/R * sign(-roll);
    else
        R = 0;
        w  = 0;
    end    
    theta = theta + w*time_res;
    
    x = x+ (v*time_res)*cos(theta);
    y = y+ (v*time_res)*sin(theta);
    
	time=time+time_res;
end


%---------z is totally independent of the other 2 axis ---------------%\
[Z,V_Z] = get_z_emergency_trajectory(z0, vz0, vz_max, acc_z, length(X), time_res);

Time = Time+repmat(time0,1,length(Time));
transition_traj = [X;Y;Z;Heading;V_XY;V_Z;Heading_rate;Time];
return;

