clear;
close all;
clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x0=0;
y0=0;
z0=0;
%vx0=50;
vz0=0;
head0=0;
roll0=0;
roll_max = 0.436%30*pi/180;
roll_rate = 0.25;%15*pi/180;
acc_xy = -1 * 0.1 *9.81;
acc_z = 0;
v_min = 20;
vz_max =0.0;
time0 = 0;
time_res = 0.1;




speeds=60:-2:20;

for i=1:length(speeds)
	[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,speeds(i),vz0,head0,roll0,roll_max,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res);
	widths(i) = abs(transition_traj(2,end)); % width equal to last y value
	
end

figure;hold on;
plot(speeds,widths)
xlabel('speed [m/s]');
ylabel('width [m]');

[xData, yData] = prepareCurveData( speeds, widths );
% Set up fittype and options.
ft = fittype( 'poly6' );
opts = fitoptions( ft );
opts.Lower = [-Inf -Inf -Inf -Inf -Inf -Inf -Inf];
opts.Upper = [Inf Inf Inf Inf Inf Inf Inf];
% Fit model to data.
[fit_width, gof] = fit( xData, yData, ft, opts );
% Plot fit with data.
figure( 'Name', 'untitled fit 1' );
h = plot( fit_width, xData, yData );
legend( h, 'widths vs. speeds', 'untitled fit 1', 'Location', 'NorthEast' );
% Label axes
xlabel( 'speeds' );
ylabel( 'widths' );
grid on



vz_max = 2.54;
acc_z = 0.7;

for i=1:length(speeds)
	[transition_traj,loiter] = generate_high_speed_emergency_trajectory(x0,y0,z0,speeds(i),vz0,head0,roll0,roll_max,roll_rate,acc_xy,acc_z,v_min,vz_max,time0,time_res);
	heights(i) = abs(transition_traj(3,end)); % width equal to last y value
	
end

figure;hold on;
plot(speeds,heights)
xlabel('speed [m/s]');
ylabel('height [m]');

[xData, yData] = prepareCurveData( speeds, heights );

% Set up fittype and options.
ft = fittype( 'poly4' );
opts = fitoptions( ft );
opts.Lower = [-Inf -Inf -Inf -Inf -Inf];
opts.Upper = [Inf Inf Inf Inf Inf];

% Fit model to data.
[fit_height, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', 'untitled fit 1' );
h = plot( fit_height, xData, yData );
legend( h, 'heights vs. speeds', 'untitled fit 1', 'Location', 'NorthEast' );
% Label axes
xlabel( 'speeds' );
ylabel( 'heights' );
grid on


save('speeds_widhts_heights.mat', 'speeds', 'widths', 'heights', 'fit_width', 'fit_height');
clear; close all;load 'speeds_widhts_heights.mat';
figure;plot( fit_height, speeds, heights);xlabel('speed[m/s]');ylabel('height[m]');
figure;plot( fit_width, speeds, widths);xlabel('speed[m/s]');ylabel('width[m]');


