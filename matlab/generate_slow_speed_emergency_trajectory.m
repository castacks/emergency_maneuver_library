function [transition_traj,loiter] = generate_slow_speed_emergency_trajectory(x0,y0,z0,vx0,vz0,head0,head_rate0,head_rate_acc,head_rate_max,acc_xy,acc_z,v_min,vz_max,time0,time_res)
%PLOTCHANGINGCURVATURETRAJECTORY Summary of this function goes here
%   Detailed explanation goes here
g = 9.80665;
v = vx0;
a = acc_xy;
az = acc_z;
vz=vz0;

x = x0;
y = y0;
theta = head0;
z = z0;
w = head_rate0;

if (abs(w)>head_rate_max)
	head_rate_max = abs(w);
end


X=[];
Y=[];
Z=[];
Heading=[];
Heading_rate=[];
time=0;% corrected at the endtime0;
V_XY=[];
V_Z=[];
Time=[];

if(vz0~=0 && acc_z==0)
	transition_traj=[];
	loiter=[];
	return;
end



while true
    X(end+1) = x;
    Y(end+1) = y;
    Heading(end+1) = theta;
    Heading_rate(end+1) = w;
    V_XY(end+1)=v;
    Time(end+1)=time;
            
	if v == v_min % we want to hover || abs(head0-theta) >= pi
%		if w > 0 % left turn
%             z_vec = [0;0;-1];
%         else
%             z_vec = [0;0;+1];
% 		end
% 		R = v / abs(w);
% 		if R ~= 0
% 			mot_dir = [cos(theta)*v;sin(theta)*v];
% 			mot_dir = mot_dir./norm(mot_dir);
% 			dir_to_center = cross([mot_dir;0],z_vec);
% 			center = dir_to_center(1:2)*abs(R) + [x;y];
% 			loiter.center = center;
% 		else
			loiter.center = [];
% 		end
        loiter.radius = 0;%abs(R);
        loiter.speed = v;
		if w < 0
			loiter.clock_wise = true;
		else
			loiter.clock_wise = false;
		end
		break;
	end
	
    v = vx0 + a*time;
    if v< v_min
        v=v_min;
	end
	
	w = w + head_rate_acc*time_res;
	if abs(w) > head_rate_max
		w = head_rate_max*sign(w);
	end
	
    theta = theta + w*time_res;
    
    x = x+ (v*time_res)*cos(theta);
    y = y+ (v*time_res)*sin(theta);
    
	time=time+time_res;
end


%---------z is totally independent of the other 2 axis ---------------%\
[Z,V_Z] = get_z_emergency_trajectory(z0, vz0, vz_max, acc_z, length(X), time_res);

Time = Time+repmat(time0,1,length(Time));
transition_traj = [X;Y;Z;Heading;V_XY;V_Z;Heading_rate;Time];
return;

