function traj = get_traj_loiter(x0,y0,z0,h0,h0_dot,v0,t0,loiter,time_res) 
	if loiter.speed == 0
		traj=[];
		return;
	end
    phi_res = v0*time_res / abs(loiter.radius);
    phi0= atan2(y0-loiter.center(2),x0-loiter.center(1));
	if loiter.clock_wise == false 
		phi = phi0:phi_res:phi0+2*pi;
	else
		phi = phi0:-phi_res:phi0-2*pi;
	end
	x = cos(phi)*loiter.radius + loiter.center(1);
    y = sin(phi)*loiter.radius + loiter.center(2);
    z = repmat(z0,1,length(x));
    head = h0:phi_res:h0+phi_res*(length(x)-1);
    % no, we do not want heading to jump!
	%head = wrapTo2Pi(head);
    head_rate = repmat(h0_dot,1,length(x));
    v_xy = repmat(v0,1,length(x));
    v_z  = zeros(1,length(x));
    % todo v_head...
    t = t0:time_res:t0+time_res*(length(x)-1);

    %% final point cause of discretization
    phi_end = phi0+2*pi;
    phi_step = phi_end - phi(end);
    time_step = phi_step*loiter.radius / v0;
    x = [x,cos(phi_end)*loiter.radius + loiter.center(1)];
    y = [y,sin(phi_end)*loiter.radius + loiter.center(2)];
    z = [z,z0];
    %head = [wrapTo2Pi(head(end) + time_step*h0_dot),head];
    head = [head,head(end) + time_step*h0_dot];
    head_rate = [h0_dot,head_rate];
    v_xy = [v_xy,v0];
    v_z = [v_z,0];
    t = [t,t(end)+time_step];

    traj = [x;y;z;head;v_xy;v_z;head_rate;t];
end