function [Z,V_Z] = get_z_emergency_trajectory(z0, vz0, vz_max, acc_z, num_pts, time_res) 
%% TODO minor error!!!, sometimes acc at end exceeds limit
z = z0;
vz = vz0;
V_Z=[];
Z=[];
acc_z
%% need to have vz=0 to have level loiter
max_time = time_res*(num_pts-1);%% -1 first point constant
% fisrt time step is constant
if acc_z < 0
	vz_max = -vz_max;
end
if max_time > (vz_max/acc_z + (vz_max - vz0)/acc_z)
%if max_time > (vz_max/acc_z+time_res + (vz_max - vz0)/acc_z)
	% enough time to reach max z velocity
	z_acc_time = (vz_max - vz0)/acc_z;
	%z_dcc_time = max_time - (vz_max/acc_z) - time_res;
	z_dcc_time = (vz_max/acc_z);
	num_acc = ceil(z_acc_time/time_res);
	num_dcc = ceil(z_dcc_time/time_res);
	dcc = (vz_max)/(num_dcc*time_res)
else
	%time_tmp = max_time - vz0/acc_z - time_res;
	time_tmp = max_time - abs(vz0/acc_z);
	if acc_z*vz0 > 0
		z_acc_time = time_tmp/2;
		z_dcc_time = time_tmp/2 + abs(vz0/acc_z);
	else
	    z_acc_time = time_tmp/2 + abs(vz0/acc_z);
		z_dcc_time = time_tmp/2;
	end
	num_acc = floor(z_acc_time/time_res);
	num_dcc = ceil(z_dcc_time/time_res);
	dcc = (vz0+num_acc*acc_z*time_res)/(num_dcc*time_res)
end

% first point
time=time_res;%Time(1);
V_Z(end+1)=vz;
Z(end+1) = z;
for i=1:num_pts-1%2:num_pts
	if i<=num_acc%time < z_acc_time 
		vz = vz+acc_z*time_res;
	elseif i>=num_pts-num_dcc%time >= z_dcc_time  % z_dcc_time
		%vz = vz-acc*time_res;
		vz = vz-dcc*time_res;
	else
		% vz is constant
	end
	%% saturate speed
	if abs(vz)>abs(vz_max)
		vz = vz_max;
	end
	
	%pos
	z =z+vz*time_res;
	time = time+time_res;
	
	V_Z(end+1)=vz;
    Z(end+1) = z;

end
lala=1;